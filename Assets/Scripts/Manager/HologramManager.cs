﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramManager : MonoBehaviour
{
    private static HologramManager instance;

    public GameObject testModel;
    public Shader maskShader;
    public Material material;

    private float lerpTime = 1;
    private float currentLerpTime;
    public bool lerpingEnabled;

    private Quaternion originRotation;
    private Vector3 originRotationVector;
    private Quaternion currentRotation;
    private Quaternion destinationRotation;
    private Vector3 destinationRotationVector;
    private Vector3 originPosition;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (lerpingEnabled)
            LerpToFocusedModelpart();
    }

    public static HologramManager GetInstance() { return instance; }

    public void SetModelsPivotToCenter(GameObject hologram)
    {
        Transform modelTransform = hologram.transform.GetChild(0);
        Renderer renderer = modelTransform.GetComponent<Renderer>();

        if (renderer == null)
            return;

        Vector3 pivot = renderer.bounds.center;
        modelTransform.transform.SetParent(Camera.main.transform);
        hologram.transform.position = pivot;
        modelTransform.SetParent(hologram.transform);
    }

    public void SetModelsInitialPosition(GameObject hologram)
    {
        GameObject mask = UIManager.GetInstance().GetMask();
        hologram.transform.position = mask.transform.position;
        float distance = 0;
        float tempDistance = 0;
        GameObject modelpart;

        if (hologram.transform.GetChild(0).GetComponent<Renderer>() == null)
        {
            int count = hologram.transform.childCount;
            for (int i = 0; i < count; i++)
            {
                modelpart = hologram.transform.GetChild(i).gameObject;
                if (modelpart.transform.childCount == 0)
                    continue;
                tempDistance = GetLongestAxisLength(modelpart.transform.GetChild(0).gameObject);
                if (tempDistance > distance)
                    distance = tempDistance;
            }
        }
        else
            distance = GetLongestAxisLength(hologram.transform.GetChild(0).gameObject);

        hologram.transform.position = new Vector3(hologram.transform.position.x, hologram.transform.position.y, hologram.transform.position.z + distance);
    }

    public float GetLongestAxisLength(GameObject model)
    {
        if (model.GetComponent<MeshRenderer>() == null)
            return 0;

        float width = model.GetComponent<MeshRenderer>().bounds.size.x;
        float height = model.GetComponent<MeshRenderer>().bounds.size.y;
        float depth = model.GetComponent<MeshRenderer>().bounds.size.z;
        float result = width;

        if (width < height)
            result = height;
        if (result < depth)
            result = depth;

        return depth;
    }

    public void InEditorSetModelsSettingsForMask(GameObject completeHologram)
    {
        int count = completeHologram.transform.childCount;
        GameObject hologram;
        GameObject model;
        Renderer renderer;
        SetModelsPivotToCenter(completeHologram);
        SetModelsInitialPosition(completeHologram);

        for (int i = 0; i < count; i++)
        {
            hologram = completeHologram.transform.GetChild(i).gameObject;
            model = hologram.transform.GetChild(0).gameObject;
            SetModelsPivotToCenter(hologram);
            renderer = hologram.GetComponentInChildren<Renderer>();
            renderer.material = material;
            renderer.material.shader = maskShader;
            model.gameObject.AddComponent<CanvasRenderer>();
            UIManager.GetInstance().AddHologramToMask(hologram);
        }
    }

    public void AddEssentialComponentsToModelpart(GameObject modelpart)
    {
        modelpart.AddComponent<MeshCollider>();
        modelpart.AddComponent<ModelPart>();
    }

    public void EnableLerpToFocusedModelpart(GameObject modelpart)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;
        GameObject hologram = hologramInstruction.GetHologram();
        lerpingEnabled = true;
        currentRotation = hologram.transform.rotation;
        hologram.transform.rotation = Quaternion.identity;
        hologram.transform.Rotate(0, 90, 90);
        Vector3 vector1 = Camera.main.transform.position - hologram.transform.position;
        Vector3 vector2 = modelpart.transform.position - hologram.transform.position;
        vector1.Normalize();
        vector2.Normalize();
        float s = Vector3.Dot(vector2, Camera.main.transform.right);
        s = Mathf.Sign(s);
        float angle = Vector3.Angle(vector1, vector2) * s;
        hologram.transform.rotation = Quaternion.identity;

        float horizontalSliderValue = UIManager.GetInstance().horizontalRotationSlider.value;
        float verticalSliderValue = UIManager.GetInstance().verticalRotationSlider.value;

        UIManager.GetInstance().horizontalRotationSlider.value = (angle / 360);
        UIManager.GetInstance().verticalRotationSlider.value = 0.5f;

        hologram.transform.Rotate(Vector3.up, angle);
        hologram.transform.Rotate(0, 90, 90);
        destinationRotation = hologram.transform.rotation;
        destinationRotationVector = destinationRotation.eulerAngles;
        hologram.transform.rotation = currentRotation;
    }

    public void LerpToFocusedModelpart()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;
        GameObject hologram = hologramInstruction.GetHologram();

        hologram.transform.rotation = currentRotation;
        currentLerpTime += Time.deltaTime;
        currentRotation = Quaternion.Lerp(currentRotation, destinationRotation, currentLerpTime / lerpTime);

        if (currentLerpTime >= lerpTime)
            ResetLerpValues();

    }

    public void ResetLerpValues()
    {
        currentLerpTime = 0;
        lerpingEnabled = false;
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;
        GameObject hologram = hologramInstruction.GetHologram();
        UIManager.GetInstance().SetSliderToRotation(hologram);
    }

    public void RotateModel()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;

        GameObject hologram = hologramInstruction.GetHologram();
        if (lerpingEnabled)
            return;

        float horizontalSliderValue = UIManager.GetInstance().horizontalRotationSlider.value;
        float verticalSliderValue = UIManager.GetInstance().verticalRotationSlider.value;
        hologram.transform.rotation = Quaternion.identity;
        float z = 0;
        if (destinationRotationVector == Vector3.zero)
            z = 90;

        Vector3 rotation = new Vector3((verticalSliderValue - 0.5f) * 360, (horizontalSliderValue - 0.5f) * 360, z);
        rotation += destinationRotationVector;
        hologram.transform.rotation = Quaternion.Euler(rotation);
    }

    public void SetModelpartsColor(GameObject modelpart, Color color)
    {
        if (modelpart.transform.childCount == 0)
            return;
        Renderer renderer = modelpart.transform.GetChild(0).GetComponent<Renderer>();
        if (renderer == null)
            return;

        Color32 color2 = renderer.material.color;
        renderer.material.color = color;
    }

    public void ResetHologramsColor(GameObject hologram)
    {
        GameObject modelpart;
        GameObject model;
        for (int i = 0; i < hologram.transform.childCount; i++)
        {
            modelpart = hologram.transform.GetChild(i).gameObject;
            if (modelpart.transform.childCount == 0)
                continue;
            model = modelpart.transform.GetChild(0).gameObject;

            SetModelpartsColor(modelpart, Color.white);
        }
    }

    //nur für Testzwecke 
    public void TestInit()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;

        GameObject hologram = hologramInstruction.GetHologram();

        InEditorSetModelsSettingsForMask(hologram);
        originPosition = hologram.transform.position;
        originRotation = hologram.transform.rotation;
        originRotationVector = originRotation.eulerAngles;

        for (int i = 0; i < hologram.transform.childCount; i++)
        {
            GameObject modelpart = hologram.transform.GetChild(i).GetChild(0).gameObject;
            AddEssentialComponentsToModelpart(modelpart);
        }

        hologram.transform.Rotate(0, 0, 90);
    }

}
