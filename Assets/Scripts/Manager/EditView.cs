﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditView : MonoBehaviour
{
    public Toggle Up_Left;
    public Toggle Up_Right;
    public Toggle Up_Middle;
    public Toggle Middle_Left;
    public Toggle Middle_Middle;
    public Toggle Middle_Right;
    public Toggle Down_Left;
    public Toggle Down_Middle;
    public Toggle Down_Right;

    public Toggle Front;
    public Toggle Middle;
    public Toggle Back;

    public Dropdown dropdownRotation;            //# 0=infinite, 1=> define Rotation by angle, 2=> define Roatation by number of Rotations
    public Dropdown dropdownRotationAxis;                //# 0=X-Axis, 1=Y-Axis, 2= Z-Axis, 3=Custom-Vector
    public Dropdown dropdownClockwise;           //# 0=Clockwise, 1=Counter Clockwise
    public Dropdown dropdownPivot;

    public InputField dropdownRotation_Inputfield;
    public InputField rotationSpeed;


    public Dropdown dropdownTranslationAxis;
    public Dropdown dropdownDirection;

    public InputField distance;

    public InputField translationSpeed;
    
    List<GameObject> modelparts = new List<GameObject>();

    public void SetMenuValues(HologramModule hologramModule)
    {
        DeactivatePivotToggles();

        if (hologramModule is HologramRotationModule)
        {
            HologramRotationModule hologramRotationModule = (HologramRotationModule)hologramModule;
            dropdownRotation.value = hologramRotationModule.dropdownRotation_value;
            dropdownRotationAxis.value = hologramRotationModule.dropdownRotationAxis_value;
            dropdownClockwise.value = hologramRotationModule.dropdownClockwise_value;
            dropdownRotation_Inputfield.text = "" + hologramRotationModule.numberOrAngle;
            rotationSpeed.text = "" + hologramRotationModule.rotationSpeed;

            switch (hologramRotationModule.pivot_X)
            {
                case -1:
                    switch (hologramRotationModule.pivot_Y)
                    {
                        case -1:
                            Down_Left.isOn = true;
                            break;
                        case 0:
                            Middle_Left.isOn = true;
                            break;
                        case 1:
                            Up_Left.isOn = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case 0:
                    switch (hologramRotationModule.pivot_Y)
                    {
                        case -1:
                            Down_Middle.isOn = true;
                            break;
                        case 0:
                            Middle_Middle.isOn = true;
                            break;
                        case 1:
                            Up_Middle.isOn = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case 1:
                    switch (hologramRotationModule.pivot_Y)
                    {
                        case -1:
                            Down_Right.isOn = true;
                            break;
                        case 0:
                            Middle_Right.isOn = true;
                            break;
                        case 1:
                            Up_Right.isOn = true;
                            break;
                        default:

                            break;
                    }
                    break;
                default:

                    break;
            }

            switch (hologramRotationModule.pivot_Z)
            {
                case -1:
                    Front.isOn = true;
                    break;
                case 0:
                    Middle.isOn = true;
                    break;
                case 1:
                    Back.isOn = true;
                    break;
                default:
                    break;
            }
        }
        else if (hologramModule is HologramTranslationModule)
        {
            HologramTranslationModule hologramTranslationModule = (HologramTranslationModule)hologramModule;
            distance.text = "" + hologramTranslationModule.distance;
            dropdownDirection.value = hologramTranslationModule.dropdownDirection_value;
            dropdownTranslationAxis.value = hologramTranslationModule.dropdownTranslationAxis_value;
            translationSpeed.text = "" + hologramTranslationModule.translationSpeed;
        }
        
    }

    public void SetValues(HologramModule hologramModule, Substep substep)
    {
        if (hologramModule is HologramRotationModule)
        {
            HologramRotationModule hologramRotationModule = (HologramRotationModule)hologramModule;
            hologramRotationModule.dropdownRotation_value = dropdownRotation.value;
            hologramRotationModule.dropdownRotationAxis_value = dropdownRotationAxis.value;
            hologramRotationModule.dropdownClockwise_value = dropdownClockwise.value;
            hologramRotationModule.numberOrAngle = float.Parse(dropdownRotation_Inputfield.text);
            hologramRotationModule.rotationSpeed = float.Parse(rotationSpeed.text);
            hologramRotationModule.SetRotationVector();
            if (Up_Left.isOn)
            {
                hologramRotationModule.pivot_X = -1;
                hologramRotationModule.pivot_Y = 1;
            }
            if (Up_Middle.isOn)
            {
                hologramRotationModule.pivot_X = 0;
                hologramRotationModule.pivot_Y = 1;
            }
            if (Up_Right.isOn)
            {
                hologramRotationModule.pivot_X = 1;
                hologramRotationModule.pivot_Y = 1;
            }
            if (Middle_Left.isOn)
            {
                hologramRotationModule.pivot_X = -1;
                hologramRotationModule.pivot_Y = 0;
            }
            if (Middle_Middle.isOn)
            {
                hologramRotationModule.pivot_X = 0;
                hologramRotationModule.pivot_Y = 0;
            }
            if (Middle_Right.isOn)
            {
                hologramRotationModule.pivot_X = 1;
                hologramRotationModule.pivot_Y = 0;
            }
            if (Down_Left.isOn)
            {
                hologramRotationModule.pivot_X = -1;
                hologramRotationModule.pivot_Y = -1;
            }
            if (Down_Middle.isOn)
            {
                hologramRotationModule.pivot_X = 0;
                hologramRotationModule.pivot_Y = -1;
            }
            if (Down_Right.isOn)
            {
                hologramRotationModule.pivot_X = 1;
                hologramRotationModule.pivot_Y = -1;
            }

            if (Front.isOn)
            {
                hologramRotationModule.pivot_Z = -1;
            }
            if (Middle.isOn)
            {
                hologramRotationModule.pivot_Z = 0;
            }
            if (Back.isOn)
            {
                hologramRotationModule.pivot_Z = 1;
            }
            SelectPivotObject(hologramRotationModule);
        }
        else if (hologramModule is HologramTranslationModule)
        {
            HologramTranslationModule hologramTranslationModule = (HologramTranslationModule)hologramModule;
            hologramTranslationModule.distance = float.Parse(distance.text);
            hologramTranslationModule.dropdownDirection_value = dropdownDirection.value;
            hologramTranslationModule.dropdownTranslationAxis_value = dropdownTranslationAxis.value;
            hologramTranslationModule.SetTranslationAxis();
            hologramTranslationModule.translationSpeed = float.Parse(translationSpeed.text);
            hologramTranslationModule.SetTranslateTerminationCondition();
        }
        
       
    }

    public void DeactivatePivotToggles()
    {
        Up_Left.isOn = false;
        Up_Right.isOn = false;
        Up_Middle.isOn = false;
        Middle_Left.isOn = false;
        Middle_Middle.isOn = false;
        Middle_Right.isOn = false;
        Down_Left.isOn = false;
        Down_Middle.isOn = false;
        Down_Right.isOn = false;
    }

    public void SelectPivotObject(HologramRotationModule hologramRotationModule)
    {
        Substep substep = ManualManager.GetInstance().GetCurrentSubstep();
        GameObject selectedModelpart;
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> complete_model_List = hologramInstruction.GetHologramsModelparts();

        if (dropdownPivot.value == 0)
            hologramRotationModule.pivotObjectindex = -1;
        else
        {
            selectedModelpart = modelparts[dropdownPivot.value - 1];
            for (int i = 0; i < complete_model_List.Count; i++)
            {
                GameObject modelpart = complete_model_List[i];
                if (Equals(complete_model_List[i], selectedModelpart))
                {
                    hologramRotationModule.pivotObjectindex = i;
                }
            }
        }
    }

    public void FillModelPartDropdown()
    {
        modelparts.Clear();
        HologramSubstep substep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        GameObject modelpart;

        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> complete_model_List = hologramInstruction.GetHologramsModelparts();

        List<string> names = new List<string>
        {
            "Eigenes Pivot"
        };

        for (int i = 0; i < complete_model_List.Count; i++)
        {
            modelpart = complete_model_List[i];

            foreach (string name in substep.GetSelectedModelpartsNames())
            {
                if (Equals(name, complete_model_List[i].transform.parent.name))
                {
                    names.Add(modelpart.transform.parent.transform.parent.name);
                }
            }
        }
        
        dropdownPivot.ClearOptions();
        dropdownPivot.AddOptions(names);
    }

}
