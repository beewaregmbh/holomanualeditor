﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    private static ApplicationManager instance;
    private ViewType currentViewType = ViewType.EDITOR;
    private CurrentView currentView = CurrentView.STEP_VIEW;
    private bool isMontage;
    public static ApplicationManager GetInstance() { return instance; }
    public bool init;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        PlattformRelatedInit();
    }

    public void SetIsMontage(bool isMontage) { this.isMontage = isMontage; }
    public bool IsMontage() { return isMontage; }

    public bool IsEditor() { return currentViewType == ViewType.EDITOR; }

    public void TestInit()
    {
        UIManager.GetInstance().instructionEditView.SetActive(true);
        UIManager.GetInstance().provisorischesStart.SetActive(false);
        ManualManager.GetInstance().TestInit();
        HologramInstruction hologramInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();
        //'has to be called where the model is loaded
        hologramInstruction.SetHologram(HologramManager.GetInstance().testModel);
        hologramInstruction.FillHologramsModelparts();


        UIManager.GetInstance().TestInit();
        HologramManager.GetInstance().TestInit();
        hologramInstruction.ComputeOriginPositionsAndRotations();

    }

    public void PlattformRelatedInit()
    {
        //#missing# Android has to be implemented if used on smartphone in the future
        //#if UNITY_ANDROID
        //currentViewType = ViewType.ANDROID;
        //#endif
#if WINDOWS_UWP
currentViewType = ViewType.HOLOLENS;
#endif
    }

    public void SetCurrentViewToStepView()
    {
        currentView = CurrentView.STEP_VIEW;
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }

    public void SetCurrentViewToAnnotationView()
    {
        currentView = CurrentView.ANNOTATION_VIEW;
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }

    MicrostepScrollPanelControl microstepScrollPanelControl;




    public void SetCurrentViewToModelpartSelectionView()
    {
        currentView = CurrentView.MODELPART_SELECTION_VIEW;
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }
    public void SetCurrentViewToMicrostepModificationView()
    {
        currentView = CurrentView.MICROSTEP_MODIFICATION_VIEW;
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }
    public void SetCurrentViewToRotationModulModificationView()
    {
        currentView = CurrentView.ROTATION_MODUL_MODIFICATION_VIEW;
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }
    public void SetCurrentViewToTranslationModulModificationView() { currentView = CurrentView.TRANSLATION_MODUL_MODIFICATION_VIEW; }
    public CurrentView GetCurrentView() { return currentView; }
}

public enum ViewType
{
    HOLOLENS,
    EDITOR,
    // ANDROID
};

public enum DataType
{
    INSTRUCTION,
    STEP,
    SUBSTEP,
    MICROSTEP,
    MODULE,
};

public enum InstructionType
{
    TEXT,
    PICTURE,
    VIDEO,
    HOLOGRAM
};

public enum CurrentView
{
    STEP_VIEW,
    ANNOTATION_VIEW,
    MODELPART_SELECTION_VIEW,
    MICROSTEP_MODIFICATION_VIEW,
    ROTATION_MODUL_MODIFICATION_VIEW,
    TRANSLATION_MODUL_MODIFICATION_VIEW,
};

