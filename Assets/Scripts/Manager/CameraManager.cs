﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private static CameraManager instance;
    Transform nearViewCameraTransform;
    Vector3 focusedModelpartPosition;
    bool lerpingEnabled;
    int direction = 0; // 0:frontal, 1:right, -1=left
    int lerpCounter = 0;
    Quaternion currentRotation;
    Quaternion previousRotation;
    Vector3 currentPosition;
    Vector3 previousPosition;
    GameObject test;

    private void Awake()
    {
        instance = this;
    }

    public static CameraManager GetInstance() { return instance; }

    void Update()
    {
        if (!lerpingEnabled || previousRotation.eulerAngles == Vector3.zero)
            return;

        if (lerpCounter < 100)
            Lerp();
        else
            LerpCompleted();

        nearViewCameraTransform.LookAt(focusedModelpartPosition);
    }


    public void LerpCompleted()
    {
        lerpingEnabled = false;
        lerpCounter = 0;
        transform.rotation = currentRotation;
    }

    public void SetPositionsAndRotations(Vector3 focusedModelpartPosition, Vector3 hologramCenter)
    {
        previousPosition = currentPosition;
        currentPosition = focusedModelpartPosition;
        this.focusedModelpartPosition = focusedModelpartPosition;
        SetPostions();
        SetRotation(hologramCenter);
    }

    public void SetPostions()
    {
        transform.position = focusedModelpartPosition;
        nearViewCameraTransform.position = new Vector3(nearViewCameraTransform.position.x, focusedModelpartPosition.y, nearViewCameraTransform.position.z);
    }

    public void SetRotation(Vector3 hologramCenter)
    {
        if (currentRotation.eulerAngles == Vector3.zero)
        {
            nearViewCameraTransform.LookAt(hologramCenter);
            currentRotation = nearViewCameraTransform.rotation;
        }

        previousRotation = currentRotation;

        if (IsLeft(hologramCenter, focusedModelpartPosition))
            transform.Rotate(new Vector3(0, -80, 0));
        else
            transform.Rotate(new Vector3(0, 80, 0));

        currentRotation = transform.rotation;
        transform.rotation = previousRotation;
        lerpingEnabled = true;
    }

    public bool IsLeft(Vector3 hologramCenter, Vector3 focusedModelpartPosition)
    {
        SetCameraToOriginRotation();
        Vector3 normedVectorX = Vector3.Normalize(transform.right);
        Vector3 vector = hologramCenter - focusedModelpartPosition;
        float dist = Vector3.Dot(normedVectorX, vector);

        if (dist > 0)
        {
            direction = 1;
            return false;
        }
        else
        {
            direction = -1;
            return true;
        }
    }

    public void SetCameraToOriginRotation()
    {
        if (direction == 1)
            transform.Rotate(new Vector3(0, -80, 0));
        else if (direction == -1)
            transform.Rotate(new Vector3(0, 80, 0));
    }

    public void Lerp()
    {
        transform.rotation = Quaternion.Lerp(previousRotation, currentRotation, 0.01f * lerpCounter);
        previousPosition = Vector3.Lerp(previousPosition, currentPosition, 0.01f * lerpCounter);
        transform.position = previousPosition;
        lerpCounter++;
    }

    public void SetNearViewCameraTransform(Transform transform) {
        nearViewCameraTransform = transform;
        nearViewCameraTransform.transform.rotation = Camera.main.transform.rotation;
    }
    
    public void SetCameraToHologram(Vector3 hologramPosition) { nearViewCameraTransform.localPosition = hologramPosition; }
    public void SetSecondCameraEnabled(bool enabled) { lerpingEnabled = enabled; }
}
