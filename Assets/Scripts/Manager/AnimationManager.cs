﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 *  In this class animations are played. There are different types of play, through which different parts of the animations
 *  can be played. 
 */
public class AnimationManager : MonoBehaviour
{
    private static AnimationManager instance;
    private Quaternion currentRotation;
    private Vector3 currentPosition;
    private bool animationEnabled;
    private PlayModus playModus;

    private HologramSubstep preAnimationCurrentSubstep;
    private HologramStep preAnimationCurrentStep;
    private HologramInstruction preAnimationCurrentInstruction;

    private HologramMicrostep startMicrostep;
    private HologramSubstep startSubstep;
    private HologramStep startStep;
    private HologramInstruction startInstruction;

    private bool isLerping;
    private void Awake()
    {
        instance = this;
    }

    private void FixedUpdate()
    {
        if (!animationEnabled)
            return;
        HologramMicrostep hologramMicrostep = (HologramMicrostep)ManualManager.GetInstance().GetCurrentMicrostep();
        HologramSubstep substep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        if (hologramMicrostep == null)
        {
            PlayNextMicrostepsAnimation();
            return;
        }
            
        hologramMicrostep.TransformModel(false, substep);
        if (hologramMicrostep.IsAnimationCompleted())
            PlayNextMicrostepsAnimation();
    }

    public static AnimationManager GetInstance() { return instance; }

    private void SetCurrent()
    {
        ManualManager manualManager = ManualManager.GetInstance();
        manualManager.SetCurrentMicrostep(startMicrostep);
        manualManager.SetCurrentSubstep(startSubstep);
        manualManager.SetCurrentStep(startStep);
        manualManager.SetCurrentInstruction(startInstruction);
    }

    public void SetPreAnimationSettings()
    {
        preAnimationCurrentSubstep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        preAnimationCurrentStep = (HologramStep)ManualManager.GetInstance().GetCurrentStep();
        preAnimationCurrentInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();
    }

    public void ResetToPreAnimationSettings()
    {
        StepScrollPanelControl stepScrollPanelControl = UIManager.GetInstance().stepScrollPanelControl;
        GameObject step = stepScrollPanelControl.GetManualElements()[preAnimationCurrentInstruction.GetStepIndex(preAnimationCurrentStep)];
        stepScrollPanelControl.SelectManualElement(step);
        SubstepScrollPanelControl substepScrollPanelControl = UIManager.GetInstance().substepScrollPanelControl;
        GameObject substep = substepScrollPanelControl.GetManualElements()[preAnimationCurrentStep.GetSubstepIndex(preAnimationCurrentSubstep)];
        substepScrollPanelControl.SelectManualElement(substep);
        ManualManager.GetInstance().SetCurrentInstruction(preAnimationCurrentInstruction);
    }

    private void EnablePlayMicrostep(HologramSubstep hologramSubstep)
    {
        if (startSubstep.GetMicrosteps().Count == 0)
        {
            PlayNextMicrostepsAnimation();
            return;
        }
          

        if (ApplicationManager.GetInstance().IsMontage())
            startMicrostep = (HologramMicrostep)startSubstep.GetMicrosteps()[startSubstep.GetMicrosteps().Count - 1];
        else
            startMicrostep = (HologramMicrostep)startSubstep.GetMicrosteps()[0];
        startMicrostep.EnableTransform();
    }

    private void EnablePlaySubstep()
    {
        startInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();
        startStep = (HologramStep)ManualManager.GetInstance().GetCurrentStep();
        startSubstep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        EnablePlayMicrostep(startSubstep);
        SetCurrent();
        animationEnabled = true;
    }

    private void EnablePlayStep()
    {
        startInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();
        startStep = (HologramStep)ManualManager.GetInstance().GetCurrentStep();

        if (startStep.GetSubsteps().Count == 0)
            return;

        if (ApplicationManager.GetInstance().IsMontage())
            startSubstep = (HologramSubstep)startStep.GetSubsteps()[startStep.GetSubsteps().Count - 1];
        else
            startSubstep = (HologramSubstep)startStep.GetSubsteps()[0];

        EnablePlayMicrostep(startSubstep);
        SetCurrent();

        animationEnabled = true;
    }

    private void EnablePlayInstruction()
    {

        startInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();

        if (ApplicationManager.GetInstance().IsMontage())
        {
            startStep = (HologramStep)startInstruction.GetSteps()[startInstruction.GetSteps().Count - 1];
            startSubstep = (HologramSubstep)startStep.GetSubsteps()[startStep.GetSubsteps().Count - 1];

        }
        else
        {
            startStep = (HologramStep)startInstruction.GetSteps()[0];
            startSubstep = (HologramSubstep)startStep.GetSubsteps()[0];
        }

        EnablePlayMicrostep(startSubstep);
        SetCurrent();
        animationEnabled = true;
    }

    public void PlaySubstep()
    {
        playModus = PlayModus.PLAY_SUBSTEP;
        SwitchPlayPause();
    }

    public void PlayStep()
    {
        playModus = PlayModus.PLAY_STEP;
        SwitchPlayPause();
    }

    public void PlayInstruction()
    {
        playModus = PlayModus.PLAY_INSTRUCTION;
        SwitchPlayPause();
    }



    public void SwitchPlayPause()
    {
        if (!animationEnabled)
        {
            animationEnabled = true;

            switch (playModus)
            {
                case PlayModus.PLAY_SUBSTEP:
                    EnablePlaySubstep();
                    break;
                case PlayModus.PLAY_STEP:
                    EnablePlayStep();
                    break;
                case PlayModus.PLAY_INSTRUCTION:
                    EnablePlayInstruction();
                    break;
                default:
                    Debug.Log("Default case");
                    break;
            }
        }
        else
        {
            if (ManualManager.GetInstance().GetCurrentMicrostep() != null)
            {
                foreach (HologramModule hologramModule in ManualManager.GetInstance().GetCurrentMicrostep().GetModules())
                {
                    hologramModule.ResetValues();
                }
            }
            animationEnabled = false;

        }
    }

    public void PlayNextMicrostepsAnimation()
    {
        HologramSubstep currentHologramSubstep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();

        if (GetCurrentMicrostepIndex() >= currentHologramSubstep.GetMicrosteps().Count - 1)
        {
            PlayNextSubstepsAnimation();
            return;
        }

        HologramMicrostep currentHologramMicrostep = (HologramMicrostep)currentHologramSubstep.GetMicrosteps()[GetCurrentMicrostepIndex() + 1];
        ManualManager.GetInstance().SetCurrentMicrostep(currentHologramMicrostep);
        currentHologramMicrostep.EnableTransform();
    }

    public void PlayNextSubstepsAnimation()
    {
        HologramStep currentHologramStep = (HologramStep)ManualManager.GetInstance().GetCurrentStep();

        if (playModus == PlayModus.PLAY_SUBSTEP)
        {
            SwitchPlayPause();
            return;
        }

        if (GetCurrentSubstepIndex() >= currentHologramStep.GetSubsteps().Count - 1)
        {
            PlayNextStepsAnimation();
            return;
        }

        HologramSubstep currentHologramSubstep = (HologramSubstep)currentHologramStep.GetSubsteps()[GetCurrentSubstepIndex() + 1];
        ManualManager.GetInstance().SetCurrentSubstep(currentHologramSubstep);
        if (currentHologramSubstep.GetMicrosteps().Count == 0)
        {
            PlayNextSubstepsAnimation();
            return;
        }
            
        HologramMicrostep currentHologramMicrostep = (HologramMicrostep)currentHologramSubstep.GetMicrosteps()[0];
        
        ManualManager.GetInstance().SetCurrentMicrostep(currentHologramMicrostep);
        currentHologramMicrostep.EnableTransform();
    }

    public void PlayNextStepsAnimation()
    {
        HologramInstruction currentHologramInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentInstruction();
        if (playModus == PlayModus.PLAY_STEP)
        {
            SwitchPlayPause();
            return;
        }

        if (GetCurrentStepIndex() >= currentHologramInstruction.GetSteps().Count - 1)
        {
            PlayNextInstructionsAnimation();
            return;
        }

        HologramStep currentHologramStep = (HologramStep)currentHologramInstruction.GetSteps()[GetCurrentStepIndex() + 1];
        ManualManager.GetInstance().SetCurrentStep(currentHologramStep);
        HologramSubstep currentHologramSubstep = (HologramSubstep)currentHologramStep.GetSubsteps()[0];
        if (currentHologramSubstep.GetMicrosteps().Count == 0)
        {
            PlayNextSubstepsAnimation();
            return;

        }
        HologramMicrostep currentHologramMicrostep = (HologramMicrostep)currentHologramSubstep.GetMicrosteps()[0];
       
        ManualManager.GetInstance().SetCurrentSubstep(currentHologramSubstep);
        ManualManager.GetInstance().SetCurrentMicrostep(currentHologramMicrostep);
        currentHologramMicrostep.EnableTransform();
    }

    public void PlayNextInstructionsAnimation()
    {
        if (playModus == PlayModus.PLAY_INSTRUCTION)
        {
            SwitchPlayPause();
            return;
        }

        // not implemented: Play all instructions
    }

    public void DemontageAnimation()
    {
        if (!animationEnabled)
            return;
        Step step = ManualManager.GetInstance().GetCurrentStep();
        if (step.GetSubsteps().Count == 0)
            return;
        if (MicrostepsAnimationCompleted())
            PlayNextMicrostepsAnimation();
    }

    public bool MicrostepsAnimationCompleted()
    {
        foreach (HologramModule module in ManualManager.GetInstance().GetCurrentMicrostep().GetModules())
        {
            if (!module.IsAnimationCompleted())
                return false;
        }

        return true;
    }

    public void SetModelsVisibility(Substep substep)
    {
        //# For Montage the modelpart should be invisible in the beginning, for demonatge they should be visible
    }

    public int GetCurrentMicrostepIndex()
    {
        Microstep currentMicrostep = ManualManager.GetInstance().GetCurrentMicrostep();
        Substep currentSubstep = ManualManager.GetInstance().GetCurrentSubstep();
        return currentSubstep.GetMicrostepIndex(currentMicrostep);
    }

    public int GetCurrentSubstepIndex()
    {
        Substep currentSubstep = ManualManager.GetInstance().GetCurrentSubstep();
        Step currentStep = ManualManager.GetInstance().GetCurrentStep();
        return currentStep.GetSubstepIndex(currentSubstep);
    }

    public int GetCurrentStepIndex()
    {
        Step currentStep = ManualManager.GetInstance().GetCurrentStep();
        Instruction currentInstruction = ManualManager.GetInstance().GetCurrentInstruction();
        return currentInstruction.GetStepIndex(currentStep);
    }
}

public enum PlayModus
{
    PLAY_SUBSTEP,
    PLAY_STEP,
    PLAY_INSTRUCTION
};