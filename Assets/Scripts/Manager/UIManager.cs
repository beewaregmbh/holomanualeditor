﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject provisorischesStart;
    public GameObject instructionEditView;
    private GameObject selectedModelpart;
    public bool enableFillList = true;
    public Sprite maskSprite;
    public GameObject rotationModulSpawner;
    public GameObject translationModulSpawner;
    public Canvas canvas;
    public GameObject stepView;
    public GameObject substepView;
    public GameObject modelpartSelectionView;
    public GameObject microstepModificationView;
    public GameObject rotationModulModificationView;
    public GameObject translationModulModificationView;
    public GameObject annotationView;

    public Button positionChangeButton;
    public Button rotationChangeButton;
    public Button scaleChangeButton;

    public Button newRotationModulButton;
    public Button newTranslationModulButton;

    private List<GameObject> listEntries = new List<GameObject>();
    private List<GameObject> microstepsEntries = new List<GameObject>();

    private Vector3 modelsPosition;
    private Quaternion modelsRotation;
    private Vector3 modelsScale;

    public InstructionScrollPanelControl instructionScrollPanelControl;
    public StepScrollPanelControl stepScrollPanelControl;
    public SubstepScrollPanelControl substepScrollPanelControl;
    public MicrostepScrollPanelControl microstepScrollPanelControl;
    public GameObject mask;

    public Slider horizontalRotationSlider;
    public Slider verticalRotationSlider;

    public GameObject listContent;

    //#used to highlight steps and selected modelparts
    private Color32 green = new Color32(138, 189, 36, 255);
    //#used for Playbutton and to highlight a preselected modelpart
    private Color32 orange = new Color32(253, 174, 89, 255);
    private Color32 almostBlack = new Color(50, 50, 50, 255);

    private static UIManager instance;

    public Sprite buttonSprite;
    public Sprite uiMaskSprite;

    private List<bool> selectedModelpartBoolList = new List<bool>();
    private bool isTempModuleSelected;

    private void Awake()
    {
        instance = this;
    }

    public static UIManager GetInstance() { return instance; }

    public void AfterDeserialisation()
    {
        GameObject stepObject;
        GameObject substep;
        SubstepScrollPanelControl substepScrollPanelControl;
        Step step;
        List<Step> steps = ManualManager.GetInstance().GetCurrentInstruction().GetSteps();
        int count = steps.Count;
        for (int i = 0; i < count; i++)
        {
            step = steps[i];
            stepObject = stepScrollPanelControl.InstantiateManualElement();
            substepScrollPanelControl = stepObject.GetComponentInChildren<SubstepScrollPanelControl>();
            stepScrollPanelControl.AddManualElement(stepObject, stepObject.GetComponent<RectTransform>().sizeDelta.x, stepObject.GetComponent<RectTransform>().sizeDelta.y);

            foreach (Substep hologramsubstep in step.GetSubsteps())
            {
                substep = substepScrollPanelControl.InstantiateManualElement();
                substepScrollPanelControl.AddManualElement(substep, substep.GetComponent<RectTransform>().sizeDelta.x, substep.GetComponent<RectTransform>().sizeDelta.y);
            }

        }
        stepScrollPanelControl.HighlightManualElement(stepScrollPanelControl.GetManualElements()[0]);
    }

    public Color32 GetGreen() { return green; }

    public Color32 GetOrange() { return orange; }

    public bool IsTempModuleSelected() { return isTempModuleSelected; }
    public void SetTempModuleSelected(bool isTempModuleSelected) { this.isTempModuleSelected = isTempModuleSelected; }

    public void AddHologramToMask(GameObject hologram)
    {
        MeshMask.GetInstance().meshFilters.Add(hologram.transform.GetChild(0).GetComponent<MeshFilter>());
        MeshMask.GetInstance().Refresh();
    }

    public GameObject GetMask() { return mask; }

    public void SetSliderToRotation(GameObject hologram)
    {
        verticalRotationSlider.value = 0.5f;
        horizontalRotationSlider.value = 0.5f;
        float horizontalSliderValue = UIManager.GetInstance().horizontalRotationSlider.value;
        float verticalSliderValue = UIManager.GetInstance().verticalRotationSlider.value;
    }

    public void TranslateModelHorizontal(GameObject hologram)
    {
        float horizontalSliderValue = horizontalRotationSlider.value;
        float verticalSliderValue = verticalRotationSlider.value;
        hologram.transform.position = new Vector3(0, 0, 0);
    }

    public void SwitchToRatation()
    {
        DeselectTransformationButtons();
        HighlightTransformationButton(rotationChangeButton);
    }

    public void DeselectTransformationButtons()
    {
        //positionChangeButton.image.color = Color.gray;
        //rotationChangeButton.image.color = Color.gray;
        //scaleChangeButton.image.color = Color.gray;
    }

    public void HighlightTransformationButton(Button button)
    {
        button.image.color = green;
    }

    public void FillListEntries()
    {

        listEntries.Clear();
        Instruction instruction = ManualManager.GetInstance().GetCurrentInstruction();

        enableFillList = false;
        if (!(instruction is HologramInstruction))
            return;

        HologramInstruction hologramInstruction = (HologramInstruction)instruction;

        foreach (GameObject modelpart in hologramInstruction.GetHologramsModelparts())
        {
            GameObject listEntry = (GameObject)Instantiate(Resources.Load("UI/Editor/ListEntry"));
            listEntry.transform.position = listContent.transform.position;
            listEntry.transform.SetParent(listContent.transform);
            listEntry.transform.localScale = listContent.transform.localScale;
            listEntry.GetComponentInChildren<Text>().text = modelpart.name;
            listEntry.GetComponentInChildren<Toggle>().onValueChanged.AddListener(delegate
            {
                if (listEntry.GetComponentInChildren<Toggle>().isOn)
                    SelectListEntry(listEntry);
                else
                    DeselectListEntry(listEntry);

            });

            listEntry.GetComponentInChildren<Button>().onClick.AddListener(delegate
            {
                ShowModelPart(listEntry.GetComponentInChildren<Button>());
            });

            listEntries.Add(listEntry);
        }
        DeselectAllModelparts();
        HologramSubstep substep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        FillSelectedModelpartBoolListFromSubstep(substep);
        InitSubstepsSelectedModelparts();
        enableFillList = true;
    }

    public void InitSubstepsSelectedModelparts()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> hologramsModelparts = hologramInstruction.GetHologramsModelparts();

        if (hologramInstruction == null)
            return;

        bool toggleValue;
        enableFillList = false;

        for (int i = 0; i < selectedModelpartBoolList.Count; i++)
        {
            toggleValue = selectedModelpartBoolList[i];
            SetToggleValue(toggleValue, listEntries[i]);

            if (toggleValue)
            {
                HighlightModelPart(hologramsModelparts[i]);
            }
        }
        enableFillList = true;
    }

    public void SetToggleValue(bool isOn, GameObject entry)
    {

        enableFillList = false;
        entry.GetComponentInChildren<Toggle>().isOn = isOn;
        enableFillList = true;

    }

    public void SelectListEntry(GameObject entry)
    {
        GameObject modelpart = GetModelpartFromEntryButton(entry.GetComponentInChildren<Button>());
        entry.GetComponentInChildren<Button>().GetComponent<Image>().sprite = buttonSprite;
        entry.GetComponentInChildren<Toggle>().image.sprite = buttonSprite;
        entry.GetComponentInChildren<Toggle>().image.color = green;
        HighlightModelPart(modelpart);

        if (enableFillList)
        {
            FillSelectedModelpartBoolListFromToggles();
            FillSelectedModelpartsFromBoolList();
        }
    }

    public void DeselectListEntry(GameObject entry)
    {
        entry.GetComponentInChildren<Button>().GetComponent<Image>().sprite = maskSprite;
        entry.GetComponentInChildren<Toggle>().image.sprite = maskSprite;
        entry.GetComponentInChildren<Toggle>().image.color = Color.white;
        GameObject modelpart = GetModelpartFromEntryButton(entry.GetComponentInChildren<Button>());
        HologramManager.GetInstance().SetModelpartsColor(modelpart, Color.white);

        if (enableFillList)
        {
            FillSelectedModelpartBoolListFromToggles();
            FillSelectedModelpartsFromBoolList();
        }
    }


    public void HighlightModelPart(GameObject modelpart)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        GameObject model = modelpart.transform.GetChild(0).gameObject;

        if (hologramInstruction == null)
            return;

        GameObject hologram = hologramInstruction.GetHologram();
        HologramManager.GetInstance().SetModelpartsColor(modelpart, green);
        HologramManager.GetInstance().EnableLerpToFocusedModelpart(modelpart);

    }

    public void ShowModelPart(GameObject modelpart)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        GameObject model = modelpart.transform.GetChild(0).gameObject;

        if (hologramInstruction == null)
            return;

        GameObject hologram = hologramInstruction.GetHologram();
        ResetEntriesColors();
        GetEntryButtonFromModelpart(modelpart).GetComponentInChildren<Text>().color = orange;
        HologramManager.GetInstance().SetModelpartsColor(modelpart, orange);
        HologramManager.GetInstance().EnableLerpToFocusedModelpart(modelpart);

        if (selectedModelpart == null)
        {
            selectedModelpart = modelpart;
            return;
        }

        HologramManager.GetInstance().ResetHologramsColor(hologram);
        HighlightModelparts();
        HologramManager.GetInstance().SetModelpartsColor(modelpart, orange);
        selectedModelpart = modelpart;
    }

    public void HighlightModelparts()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> hologramsModelparts = hologramInstruction.GetHologramsModelparts();
        if (hologramInstruction == null)
            return;

        bool toggleValue;
        enableFillList = false;
        for (int i = 0; i < selectedModelpartBoolList.Count; i++)
        {
            toggleValue = selectedModelpartBoolList[i];
            SetToggleValue(toggleValue, listEntries[i]);
            if (toggleValue)
            {
                HighlightModelPart(hologramsModelparts[i]);
            }

        }
        enableFillList = true;
    }

    public void ShowModelPart(Button modelpartsListEntryButton)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        if (hologramInstruction == null)
            return;
        GameObject hologram = hologramInstruction.GetHologram();

        GameObject modelpart = GetModelpartFromEntryButton(modelpartsListEntryButton);
        if (modelpart.transform.childCount == 0)
            return;
        GameObject model = modelpart.transform.GetChild(0).gameObject;

        if (modelpart == null)
            return;

        ResetEntriesColors();
        modelpartsListEntryButton.GetComponentInChildren<Text>().color = orange;

        HologramManager.GetInstance().SetModelpartsColor(modelpart, orange);
        HologramManager.GetInstance().EnableLerpToFocusedModelpart(modelpart);
        if (selectedModelpart == null)
        {
            selectedModelpart = modelpart;
            return;
        }
        HologramManager.GetInstance().ResetHologramsColor(hologram);
        HighlightModelparts();
        HologramManager.GetInstance().SetModelpartsColor(modelpart, orange);
        selectedModelpart = modelpart;
    }

    public void ResetEntriesColors()
    {
        Button button;

        foreach (GameObject entry in listEntries)
        {
            button = entry.GetComponentInChildren<Button>();
            button.GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public GameObject GetModelpartFromEntryButton(Button modelpartsListEntryButton)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();

        if (hologramInstruction == null)
            return null;

        foreach (GameObject modelpart in hologramInstruction.GetHologramsModelparts())
        {
            if (Equals(modelpart.name, modelpartsListEntryButton.GetComponentInChildren<Text>().text))
                return modelpart;
        }

        return null;
    }

    public Button GetEntryButtonFromModelpart(GameObject modelpart)
    {
        Button button;

        foreach (GameObject entry in listEntries)
        {
            button = entry.GetComponentInChildren<Button>();
            if (Equals(modelpart.name, button.GetComponentInChildren<Text>().text))
                return button;
        }

        return null;
    }

    public void SelectAllModelparts()
    {
        enableFillList = false;

        foreach (GameObject entry in listEntries)
        {
            SetToggleValue(true, entry);
        }

        enableFillList = true;
    }

    public void DeselectAllModelparts()
    {
        GameObject entry;
        enableFillList = false;
        for (int i = 0; i < listEntries.Count; i++)
        {
            entry = listEntries[i];
            entry.GetComponentInChildren<Button>().gameObject.GetComponent<Image>().sprite = maskSprite;
            entry.GetComponentInChildren<Toggle>().image.sprite = maskSprite;
            SetToggleValue(false, entry);
        }
        enableFillList = true;
    }

    public List<GameObject> GetListEntries() { return listEntries; }

    //nur für Testzwecke 
    public void TestInit()
    {
        FillListEntries();
        stepScrollPanelControl.CreateManualElement(0);
    }

    public void SwitchViewInHologramInstruction()
    {

        CurrentView currentView = ApplicationManager.GetInstance().GetCurrentView();
        DisableViews();
        if (currentView == CurrentView.STEP_VIEW)
        {
            stepView.SetActive(true);
        }
        else if (currentView == CurrentView.MODELPART_SELECTION_VIEW)
        {
            FillListEntries();
            modelpartSelectionView.SetActive(true);
            substepView.SetActive(true);
        }
        else if (currentView == CurrentView.MICROSTEP_MODIFICATION_VIEW)
        {
            microstepScrollPanelControl.FillScrollPanel();
            microstepModificationView.SetActive(true);
            substepView.SetActive(true);
        }
        else if (currentView == CurrentView.ROTATION_MODUL_MODIFICATION_VIEW)
        {
            rotationModulModificationView.SetActive(true);
            substepView.SetActive(true);
        }
        else if (currentView == CurrentView.TRANSLATION_MODUL_MODIFICATION_VIEW)
        {
            translationModulModificationView.SetActive(true);
            substepView.SetActive(true);
        }
        else if (currentView == CurrentView.ANNOTATION_VIEW)
        {
            annotationView.SetActive(true);
            substepView.SetActive(true);
        }
    }

    public void DisableViews()
    {
        stepView.SetActive(false);
        substepView.SetActive(false);
        modelpartSelectionView.SetActive(false);
        microstepModificationView.SetActive(false);
        rotationModulModificationView.SetActive(false);
        translationModulModificationView.SetActive(false);
        annotationView.SetActive(false);
        ClearMainList();
    }

    public void ClearMainList()
    {
        for (int i = 0; i < listContent.transform.childCount; i++)
        {
            Destroy(listContent.transform.GetChild(i).gameObject);
            microstepScrollPanelControl.GetManualElements().Clear();
        }

    }

    public void FillSelectedModelpartBoolListFromToggles()
    {
        selectedModelpartBoolList.Clear();
        Toggle toggle;

        foreach (GameObject listEntry in UIManager.GetInstance().GetListEntries())
        {
            toggle = listEntry.GetComponentInChildren<Toggle>();
            if (toggle.isOn)
                selectedModelpartBoolList.Add(true);
            else
                selectedModelpartBoolList.Add(false);
        }
    }

    public List<bool> FillSelectedModelpartBoolListFromSubstep(HologramSubstep substep)
    {
        Instruction instruction = ManualManager.GetInstance().GetCurrentInstruction();

        if (!(instruction is HologramInstruction))
            return null;

        if (substep == null)
            return null;

        HologramInstruction hologramInstruction = (HologramInstruction)instruction;
        List<GameObject> hologramsModelparts = hologramInstruction.GetHologramsModelparts();
        selectedModelpartBoolList.Clear();
        GameObject modelpart;
        List<string> selectedModelparts = substep.GetSelectedModelpartsNames();

        for (int i = 0; i < hologramsModelparts.Count; i++)
        {
            modelpart = hologramsModelparts[i];
            selectedModelpartBoolList.Add(false);

            foreach (string name in selectedModelparts)
            {
                if (Equals(modelpart.name, name))
                {
                    selectedModelpartBoolList[i] = true;
                    break;
                }
            }
        }
        return selectedModelpartBoolList;
    }

    public void FillSelectedModelpartsFromBoolList()
    {
        HologramSubstep substep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();

        if (substep == null)
            return;

        substep.GetSelectedModelpartsNames().Clear();
        substep.GetSelectedModelparts().Clear();
        GameObject listEntry;
        Toggle toggle;
        List<string> selectedModelparts = substep.GetSelectedModelpartsNames();

        for (int i = 0; i < selectedModelpartBoolList.Count; i++)
        {
            listEntry = listEntries[i];
            toggle = listEntry.GetComponentInChildren<Toggle>();
            if (toggle.isOn)
            {
                substep.GetSelectedModelpartsNames().Add(listEntry.GetComponentInChildren<Text>().text);

                foreach (GameObject modelpart in hologramInstruction.GetHologramsModelparts())
                {
                    if (Equals(modelpart.name, listEntry.GetComponentInChildren<Text>().text))
                    {
                        substep.GetSelectedModelparts().Add(modelpart);
                        break;
                    }
                }
            }
        }
    }
}


