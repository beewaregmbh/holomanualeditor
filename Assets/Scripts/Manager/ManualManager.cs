﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//# Class which manages which manual is active, manages data to the current manual (name, path etc.) and the current state (current Instruction, current step etc.)

public class ManualManager
{
    private List<Instruction> instructions = new List<Instruction>();
    private long currentInstructionID = 0;
    private long currentStepID = 0;
    private static ManualManager instance;
    private Manual currentManual;
    private Instruction currentInstruction;
    private Step currentStep;
    private Substep currentSubstep;
    private Microstep currentMicrostep;
    private Module currentModule;

    private ManualManager()
    {
        
    }

    public static ManualManager GetInstance()
    {
        if (instance == null)
            instance = new ManualManager();

        return instance;
    }

    public Manual NewManual(string name)
    {
        Manual result = new Manual();
        currentManual = result;
        return result;
    }

    public HologramInstruction GetCurrentHologrammInstruction()
    {
        Instruction instruction = ManualManager.GetInstance().GetCurrentInstruction();

        if (!(instruction is HologramInstruction))
            return null;
        else
        return (HologramInstruction)instruction;

    }

    public Manual GetCurrentManual() {return currentManual;}

    public void SetCurrentManual(Manual currentManual) {this.currentManual = currentManual;}

    public Instruction GetCurrentInstruction() {return currentInstruction;}

    public void SetCurrentInstruction(Instruction currentInstruction) {this.currentInstruction = currentInstruction;}

    public Step GetCurrentStep() {return currentStep;}

    public void SetCurrentStep(Step currentStep) {this.currentStep = currentStep;}
    public void SetCurrentStep(int index) {currentStep = currentInstruction.GetSteps()[index];}

    public Substep GetCurrentSubstep() {return currentSubstep;}

    public void SetCurrentSubstep(Substep currentSubstep) {this.currentSubstep = currentSubstep;}
    public void SetCurrentSubstep(int index) { currentSubstep = currentStep.GetSubsteps()[index]; }

    public Microstep GetCurrentMicrostep() {return currentMicrostep;}

    public void SetCurrentMicrostep(Microstep currentMicrostep) {this.currentMicrostep = currentMicrostep;}

    public Module GetCurrentModule() {return currentModule;}

    public void SetCurrentModule(Module currentModule) {this.currentModule = currentModule;}

    public long GetNextStepID() {return currentStepID++;}


    public void TestInit()
    {
        currentManual = NewManual("");
        currentManual.CreateNewHologramInstruction(0);
    }

    public void ComputeDemonatageOriginPosition()
    {
        HologramInstruction currentHologramInstruction = GetCurrentHologrammInstruction();
        GameObject hologram = currentHologramInstruction.GetHologram();
        Vector3 currentPosition = new Vector3(hologram.transform.position.x, hologram.transform.position.y, hologram.transform.position.z);
        Quaternion currentRotation =new Quaternion( hologram.transform.rotation.x, hologram.transform.rotation.y, hologram.transform.rotation.z, hologram.transform.rotation.w);
        //# missing - is it possible to have instructions with different models? -> just those instructions with the same model
        List<HologramInstruction> hologramInstructions = new List<HologramInstruction>();

        ResetHologramToInitalTransform();
        foreach (Instruction instruction in ManualManager.GetInstance().GetCurrentManual().GetInstructions())
        {
            if (instruction is HologramInstruction)
                hologramInstructions.Add((HologramInstruction)instruction);
        }
        if (hologramInstructions.Count == 0)
            return;
        HologramInstruction firstHologramInstruction = hologramInstructions[0];
        foreach (HologramInstruction hologramInstruction in hologramInstructions)
        {
            //hologramInstruction.PreTransform();
            foreach (HologramStep step in hologramInstruction.GetSteps())
            {
                foreach (HologramSubstep substep in step.GetSubsteps())
                {
                    foreach (HologramMicrostep miscrostep in substep.GetMicrosteps())
                    {
                        miscrostep.GetOriginPositions().Clear();
                        miscrostep.GetOriginRotations().Clear();
                        foreach (GameObject modelpart in substep.GetSelectedModelparts())
                        {
                            hologramInstruction.PreTransform();
                            miscrostep.GetOriginPositions().Add(modelpart.transform.position);
                            miscrostep.GetOriginRotations().Add(modelpart.transform.rotation);
                            hologramInstruction.PostTransform();
                            miscrostep.TransformModel(true, substep);
                        }

                    }
                }
            }
        }
       
        GameObject modelpart1;
        currentHologramInstruction.GetHologram().transform.position = firstHologramInstruction.GetOriginPosition();
        currentHologramInstruction.GetHologram().transform.rotation = firstHologramInstruction.GetOriginRotation();
        for (int i = 0, count = firstHologramInstruction.GetHologramsModelparts().Count; i<count;i++)
        {
            modelpart1 = firstHologramInstruction.GetHologramsModelparts()[i];
            modelpart1.transform.rotation = firstHologramInstruction.GetOriginRotations()[i];
            modelpart1.transform.position = firstHologramInstruction.GetOriginPositions()[i];
        }

        currentHologramInstruction.GetHologram().transform.position = currentPosition;
        currentHologramInstruction.GetHologram().transform.rotation = currentRotation;
    }

    public void AfterDeserializationInEditor(Manual manual)
    {
        currentManual = manual;
        if (manual.GetInstructions().Count == 0)
            return;
        currentInstruction = manual.GetInstructions()[0];
        if (currentInstruction.GetSteps().Count == 0)
            return;
        currentStep = currentInstruction.GetSteps()[0];
        if (currentStep.GetSubsteps().Count == 0)
            return;
        currentSubstep = currentStep.GetSubsteps()[0];
        
        //Test 
        GetCurrentHologrammInstruction().SetHologram(HologramManager.GetInstance().testModel);
        GetCurrentHologrammInstruction().FillHologramsModelparts();
    }

    public void ResetHologramToInitalTransform()
    {
        HologramInstruction hologramInstruction = GetCurrentHologrammInstruction();
        GameObject modelpart;
        List<GameObject> modelparts = hologramInstruction.GetHologramsModelparts();
        hologramInstruction.PreTransform();
        for (int i = 0; i < modelparts.Count; i++)
        {
            modelpart = modelparts[i];
            modelpart.transform.position = hologramInstruction.GetOriginPositions()[i];
            modelpart.transform.rotation = hologramInstruction.GetOriginRotations()[i];
        }
        hologramInstruction.PostTransform();
    }

}
