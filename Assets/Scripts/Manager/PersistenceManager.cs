﻿using System.IO;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

public class PersistenceManager : MonoBehaviour
{
    private string currentSaveFile;
    private string fileName;
    private string resourcePath;
    string path;
    
    void Start()
    {
        Test();
    }

    public void Test()
    {
        resourcePath = "Saves\\";
        path = Application.dataPath + "\\Resources\\" + resourcePath;
    }
    
    public void SerialzeInEditor()
    {
        fileName = "test";
        string completePath = path + fileName + ".xml";
        HologramInstruction hologramInstruction = new HologramInstruction();
        hologramInstruction.GetSteps().Add(new HologramStep());
        XmlSerializer serializer = new XmlSerializer(typeof(Manual));
        StreamWriter streamWriter = new StreamWriter(new FileStream(completePath, FileMode.Create));
        Manual manual = ManualManager.GetInstance().GetCurrentManual();
        if (streamWriter != null)
            serializer.Serialize(streamWriter, manual);

        streamWriter.Dispose();
#if !WINDOWS_UWP

        AssetDatabase.Refresh();
#endif
    }
    public void DezerializeInEditor()
    {
        UIManager uiManager = UIManager.GetInstance();
        ApplicationManager.GetInstance().init = true;
        fileName = "test";
        TextAsset textAsset = (TextAsset)Resources.Load(resourcePath + fileName);
        XmlSerializer deserializer = new XmlSerializer(typeof(Manual));
        StringReader reader = new StringReader(textAsset.text);
        Manual manual = (Manual)deserializer.Deserialize(reader);
        ManualManager.GetInstance().AfterDeserializationInEditor(manual);
        uiManager.AfterDeserialisation();

        //just for testing
        uiManager.instructionEditView.SetActive(true);
        ApplicationManager.GetInstance().init = false;

    }
}
