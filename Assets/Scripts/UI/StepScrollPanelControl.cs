﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StepScrollPanelControl : ScrollPanelController
{
    private GameObject content;

    public override GameObject CreateManualElement(int index)
    {
        GameObject manualElement = null;
        if (ApplicationManager.GetInstance().IsEditor())
        {
            manualElement = InstantiateManualElement();
            CreateNewManualElementDatastructure(index, manualElement);
            Instruction currentInstruction = ManualManager.GetInstance().GetCurrentInstruction();
            manualElement.name += currentInstruction.GetSteps()[index].GetID();
            HighlightManualElement(manualElement);
            GetPlayButton(manualElement).onClick.AddListener(() => AnimationManager.GetInstance().PlayStep());
        }
        
        return manualElement;
    }

    public override GameObject CreateManualElement(int index, InstructionType instructionType)
    {
        throw new System.NotImplementedException();
    }

    public override void SelectManualElement(GameObject manualElement)
    {
        int index = GetManualElementsIndex(manualElement);
        ManualManager.GetInstance().SetCurrentStep(index);
        //ApplicationManager.GetInstance().SetCurrentViewToStepView();
        SubstepScrollPanelControl substepScrollPanelControl = manualElement.GetComponentInChildren<SubstepScrollPanelControl>();
        List<GameObject> substeps = substepScrollPanelControl.GetManualElements();
        
        if (substeps.Count == 0 )
            substepScrollPanelControl.CreateManualElement();
        if (ApplicationManager.GetInstance().IsMontage())
            substepScrollPanelControl.HighlightManualElement(substeps[substeps.Count - 1]);
        else
            substepScrollPanelControl.HighlightManualElement(substeps[0]);
        if (CanBeAnimated())
            GetPlayButton(manualElement).interactable = true;
        else
            GetPlayButton(manualElement).interactable = false;

        
    }

    public bool CanBeAnimated()
    {         
        List<Microstep> microsteps =  ManualManager.GetInstance().GetCurrentSubstep().GetMicrosteps();
        return microsteps.Count > 0;  
    }

    public GameObject GetSubstepScrollPanel(GameObject manualElement)
    {
        return manualElement.transform.GetChild(5).gameObject;
    }

    public override Button GetElementsAddButton(GameObject manualElement) { return manualElement.transform.GetChild(3).GetComponent<Button>(); }

    public override Button GetElementsDeleteButton(GameObject manualElement) { return manualElement.transform.GetChild(4).GetComponent<Button>(); }

    public override GameObject CreateManualElement()
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificHighlight(GameObject manualElement)
    {
        GetElementsPanel(manualElement).GetComponent<Image>().color = UIManager.GetInstance().GetGreen();
        GetImageObject(manualElement).SetActive(false);
        SelectManualElement(manualElement);
    }

    public override void TypeSpecificDeselection(GameObject manualElement, int i)
    {
        GameObject imageObject;
        imageObject = GetImageObject(manualElement);
        imageObject.SetActive(true);
        GetElementsTitle(imageObject).text = "" + (i + 1);
    }

    public override GameObject GetElementsPanel(GameObject manualElement)
    {
        if (manualElement == null)
            return null;

        GameObject result = manualElement.transform.GetChild(0).gameObject;
        return result;
    }
    public override GameObject GetImageObject(GameObject manualElement)
    {
        if (manualElement == null)
            return null;

        GameObject result = manualElement.transform.GetChild(6).gameObject;
        return result;
    }

    public override Text GetElementsTitle(GameObject imageObject)
    {
        if (imageObject == null)
            return null;

        Text result = imageObject.transform.GetChild(0).GetComponent<Text>();
        return result;
    }

    public override GameObject GetManualElement(PointerEventData eventData)
    {
        return eventData.selectedObject.transform.parent.gameObject;
    }

    public override GameObject InstantiateManualElement(int index)
    {
        throw new System.NotImplementedException();

    }

    public override void CreateNewManualElementDatastructure(int index,GameObject manualElement)
    {
        float width = GetElementsPanel(manualElement).GetComponent<RectTransform>().sizeDelta.x;
        float height = GetElementsPanel(manualElement).GetComponent<RectTransform>().sizeDelta.y;

        if (index < manualElements.Count - 1)
            manualElements.Insert(index + 1, manualElement);
        else
            manualElements.Add(manualElement);

        ManualManager manualManager = ManualManager.GetInstance();
        Instruction currentInstruction = manualManager.GetCurrentInstruction();
        Step step = currentInstruction.CreateNewStep(index);
        manualManager.SetCurrentStep(step);
    }

    public override GameObject InstantiateManualElement()
    {
        GameObject manualElement = (GameObject)GameObject.Instantiate(Resources.Load("UI/Editor/EditorHologrammStep"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        SetManualElementToScrollerContent(manualElement);
        GetElementsPanel(manualElement).AddComponent<SelectManualElement>();
        GetElementsPanel(manualElement).GetComponent<SelectManualElement>().SetScrollPanelController(this);
        GetImageObject(manualElement).AddComponent<SelectManualElement>();
        GetImageObject(manualElement).GetComponent<SelectManualElement>().SetScrollPanelController(this);
        GetElementsAddButton(manualElement).gameObject.AddComponent<AddManuelElement>();
        GetElementsAddButton(manualElement).gameObject.GetComponent<AddManuelElement>().SetScrollPanelController(this);
        GetElementsDeleteButton(manualElement).gameObject.AddComponent<DeleteManualElement>();
        GetElementsDeleteButton(manualElement).GetComponent<DeleteManualElement>().SetScrollPanelController(this);
        SubstepScrollPanelControl substepScroller = GetSubstepScrollPanel(manualElement).GetComponent<SubstepScrollPanelControl>();
        substepScroller.GetElementsAddButton(this.gameObject).onClick.AddListener(() => substepScroller.CreateManualElement());
        substepScroller.GetElementsDeleteButton(this.gameObject).onClick.AddListener(() => substepScroller.DeleteManualElement());
        return manualElement;
    }

    public override void CreateNewManualElementDatastructure(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public Button GetPlayButton(GameObject manualElement)
    {
        return manualElement.transform.GetChild(2).GetComponent<Button>();
    }
}
