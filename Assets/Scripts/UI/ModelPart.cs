﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelPart : MonoBehaviour
{

    private bool isSelected;

    public bool IsSelected() { return isSelected; }

    private void OnMouseUp()
    {
        UIManager.GetInstance().ShowModelPart(this.gameObject.transform.parent.gameObject);
    }

}
