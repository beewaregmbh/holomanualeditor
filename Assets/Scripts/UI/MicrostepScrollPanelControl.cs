﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MicrostepScrollPanelControl : ScrollPanelController
{

    private bool isDragTempModuleEnabled;
    private bool tempIsRotationModule;
    private GameObject tempMicrostep;


    public override GameObject CreateManualElement(int index)
    {
        GameObject manualElement = InstantiateManualElement();
        CreateNewManualElementDatastructure(index, manualElement);
        return manualElement;
    }

    public bool IsPointerOverMicrostep(Vector3 pos, GameObject tempMicrostep)
    {
        GameObject closestManualElement = GetClosestManualElementToPosition(pos, tempMicrostep);
        float distanceY = Mathf.Abs(closestManualElement.transform.position.y - pos.y);
        int index = GetManualElementsIndex(closestManualElement);
        float height = closestManualElement.transform.GetChild(0).GetComponent<Image>().sprite.border.y;
        return (distanceY < 0.5 * height);
    }

    public void AddTempModuleToClosestOrNewMicrostep(Vector3 pos, GameObject tempModule)
    {
        GameObject closestManualElement = GetClosestManualElementToPosition(pos, tempMicrostep);
        if (IsPointerOverMicrostep(pos, tempMicrostep) || ManualManager.GetInstance().GetCurrentMicrostep().GetModules().Count == 0)
        {
            ModuleScrollPanelControl moduleScrollPanelControl = closestManualElement.GetComponentInChildren<ModuleScrollPanelControl>();
            moduleScrollPanelControl.SetManualElementToScrollerContent(tempModule);

            RemoveTempMicrostep();
           
        }
        else
        {
            ModuleScrollPanelControl moduleScrollPanelControl;

            if (tempMicrostep == null)
            {
                int index = GetManualElementsIndex(closestManualElement);
                tempMicrostep = CreateManualElement(index+1);
                moduleScrollPanelControl = tempMicrostep.GetComponentInChildren<ModuleScrollPanelControl>();
                int siblingsIndex = GetManualElementsSiblingIndex(tempMicrostep);
                tempMicrostep.transform.SetSiblingIndex(siblingsIndex + 1);
            }

            moduleScrollPanelControl = tempMicrostep.GetComponentInChildren<ModuleScrollPanelControl>();
            moduleScrollPanelControl.SetManualElementToScrollerContent(tempModule);
        }
    }

    public void RemoveTempMicrostep()
    {
        if (tempMicrostep != null)
            RemoveManualElement(tempMicrostep);
        tempMicrostep = null;
    }

    public void AddTempModuleToNewMicrostepAfterClosestMicrostep(Vector3 pos, GameObject tempModule)
    {
        ModuleScrollPanelControl moduleScrollPanelControl = GetClosestManualElementToPosition(pos, null).GetComponentInChildren<ModuleScrollPanelControl>();
        moduleScrollPanelControl.SetManualElementToScrollerContent(tempModule);
    }


    public override GameObject CreateManualElement()
    {
        GameObject manualElement = InstantiateManualElement();
        CreateNewManualElementDatastructure(manualElement);
        return manualElement;
    }

    public override GameObject InstantiateManualElement()
    {
        GameObject manualElement = (GameObject)GameObject.Instantiate(Resources.Load("UI/Editor/Microstep"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        manualElement.transform.SetParent(GetContent().transform);
        manualElement.transform.position = GetContent().transform.position;
        manualElement.transform.rotation = GetContent().transform.rotation;
        manualElement.transform.localScale = GetContent().transform.localScale;
        return manualElement;
    }

    public void FillScrollPanel()
    {
        HologramSubstep hologramSubstep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        HologramMicrostep hologramMicrostep;
        GameObject microstep;
        ModuleScrollPanelControl moduleScrollPanelControl;
        manualElements.Clear();
        if (ManualManager.GetInstance().GetCurrentSubstep().GetMicrosteps().Count == 0)
        {
            CreateManualElement();
            return;
        }

        for (int i = 0; i < hologramSubstep.GetMicrosteps().Count; i++)
        {
            hologramMicrostep = (HologramMicrostep)hologramSubstep.GetMicrosteps()[i];
            microstep = InstantiateManualElement();
            manualElements.Add(microstep);
            moduleScrollPanelControl = microstep.GetComponent<ModuleScrollPanelControl>();
            foreach (HologramModule module in hologramMicrostep.GetModules())
            {
                if (module is HologramRotationModule)
                    moduleScrollPanelControl.GetManualElements().Add( moduleScrollPanelControl.InstantiateRotationModule());
                else
                    moduleScrollPanelControl.GetManualElements().Add(moduleScrollPanelControl.InstantiateTranslationModule());
                
            }
        }
       
    }

    public override void CreateNewManualElementDatastructure(GameObject manualElement)
    {
        Microstep microstep = ManualManager.GetInstance().GetCurrentSubstep().CreateNewMicrostep();
        manualElements.Add(manualElement);
        ManualManager.GetInstance().SetCurrentMicrostep(microstep);
    }

    public override GameObject CreateManualElement(int index, InstructionType instructionType)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsAddButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsDeleteButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetElementsPanel(GameObject manualElement)
    {
        return manualElement;
    }

    public override Text GetElementsTitle(GameObject imageObject)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetImageObject(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetManualElement(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public override void SelectManualElement(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificDeselection(GameObject manualElement, int i)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificHighlight(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override void CreateNewManualElementDatastructure(int index, GameObject manualElement)
    {
        ManualManager.GetInstance().GetCurrentSubstep().CreateNewMicrostep(index);
        manualElements.Insert(index, manualElement);
    }

    public override GameObject InstantiateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public void SetTempMicrostep(GameObject tempMicrostep) { this.tempMicrostep = tempMicrostep; }
}
