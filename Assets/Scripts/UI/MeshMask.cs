﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;


//# Core Code from:  https://forum.unity3d.com/threads/ui-and-masking-3d-meshes-in-a-scrollrect.358475/

//# can't be used for HoloLensview -> ruckelt :(
[ExecuteInEditMode]
public class MeshMask : MonoBehaviour {

    public List<MeshFilter> meshFilters = new List<MeshFilter>();
    private static MeshMask instance;

    private void Awake()
    {
        instance = this;
    }

    public static MeshMask GetInstance()
    {
        if (instance == null)
            instance = new MeshMask();
        return instance;
    }

    public void Refresh()
    {
        CanvasRenderer canvasRenderer;
        List<Material> materials;

        foreach (MeshFilter meshFilter in meshFilters)
        {
            if (meshFilter == null)
                continue;

            canvasRenderer = meshFilter.transform.GetComponent<CanvasRenderer>();
            canvasRenderer.SetMesh(meshFilter.sharedMesh);
            materials = meshFilter.transform.GetComponent<Renderer>().sharedMaterials.ToList();

            for (int i = 0; i < materials.Count; i++)
            {
                canvasRenderer.materialCount = materials.Count;
                canvasRenderer.SetMaterial(materials[i], i);
            }
        }
    }
}
