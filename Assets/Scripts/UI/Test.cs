﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
    public GameObject list;
    public List<GameObject> objectList;

	void Start () {

        for (int i = 0; i <10; i++)
        {
            ListTest();
        }

    }

    public void ListTest()
    {
        GameObject gameobject = (GameObject)GameObject.Instantiate(Resources.Load("UI/HologrammStep"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        objectList.Add(gameobject);
        gameobject.transform.SetParent(list.transform);
        float width = 0;

        for (int i = 0; i < objectList.Count; i++)
        {
            objectList[i].transform.position = list.transform.position + new Vector3( width, list.GetComponent<RectTransform>().sizeDelta.y, 0);
            width += objectList[i].transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
        }
       
    }
}
