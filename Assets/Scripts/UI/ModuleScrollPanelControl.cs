﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModuleScrollPanelControl : ScrollPanelController
{
    private GameObject tempModule;

    public void CreateManualElement(bool isRotationModule)
    {
        GameObject manualElement;
        if (isRotationModule)
            manualElement = InstantiateRotationModule();
        else
            manualElement = InstantiateTranslationModule();
        CreateNewManualElementDatastructure(manualElement, isRotationModule);
        
    }

    public GameObject InstantiateRotationModule()
    {
        GameObject manualElement = (GameObject)GameObject.Instantiate(Resources.Load("UI/Editor/RotationModule"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        SetManualElementToScrollerContent(manualElement);
        manualElements.Add(manualElement);
        return manualElement;
    }

    public GameObject InstantiateTranslationModule()
    {
        GameObject manualElement = (GameObject)GameObject.Instantiate(Resources.Load("UI/Editor/TranslationModule"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        SetManualElementToScrollerContent(manualElement);
        manualElements.Add(manualElement);
        return manualElement;

    }

    public override GameObject CreateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject CreateManualElement(int index, InstructionType instructionType)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject CreateManualElement()
    {
       return InstantiateManualElement();
    }

    public override void CreateNewManualElementDatastructure(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public  void CreateNewManualElementDatastructure(GameObject manualElement, bool isRotationModul)
    {
        ManualManager manualManager = ManualManager.GetInstance();
        Microstep currentMicrostep = manualManager.GetCurrentMicrostep();
        Module module = currentMicrostep.CreateNewModule(isRotationModul);
        manualElement.name += currentMicrostep.GetModules().Count;
        manualManager.SetCurrentModule(module);
    }

    public override void CreateNewManualElementDatastructure(int index, GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsAddButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsDeleteButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetElementsPanel(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Text GetElementsTitle(GameObject imageObject)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetImageObject(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetManualElement(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void CreateTempModule()
    {
        tempModule = CreateManualElement();
    }

    public override GameObject InstantiateManualElement()
    {
        throw new System.NotImplementedException();
    }

    public override GameObject InstantiateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public override void SelectManualElement(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificDeselection(GameObject manualElement, int i)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificHighlight(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

}
