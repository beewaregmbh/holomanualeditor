﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SubstepScrollPanelControl : ScrollPanelController
{
    private Button addButton;
    private Button deleteButton;

    public override GameObject CreateManualElement(int index)
    {
        GameObject substepElement = InstantiateManualElement();
        float width = GetElementsPanel(substepElement).GetComponent<RectTransform>().sizeDelta.x;
        float height = GetElementsPanel(substepElement).GetComponent<RectTransform>().sizeDelta.y;
        manualElements.Insert(index,substepElement);
        CreateNewManualElementDatastructure(substepElement);
        HighlightManualElement(substepElement);
        return substepElement;
    }

    public override GameObject CreateManualElement(int index, InstructionType instructionType)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject CreateManualElement()
    {
        GameObject substepElement = InstantiateManualElement();
        float width = GetElementsPanel(substepElement).GetComponent<RectTransform>().sizeDelta.x;
        float height = GetElementsPanel(substepElement).GetComponent<RectTransform>().sizeDelta.y;
        manualElements.Add(substepElement);
        CreateNewManualElementDatastructure(substepElement);
        HighlightManualElement(substepElement);
        return substepElement;
    }

    public override void SelectManualElement(GameObject manualElement)
    {
        int index = GetManualElementsIndex(manualElement);
        ManualManager.GetInstance().SetCurrentSubstep(index);
        ApplicationManager.GetInstance().SetCurrentViewToModelpartSelectionView();
    }

    public override Button GetElementsAddButton(GameObject manualElement)
    {
        return this.transform.GetChild(3).GetComponent<Button>();
    }

    public override Button GetElementsDeleteButton(GameObject manualElement)
    {
        return this.transform.GetChild(2).GetComponent<Button>();
    }

    public void DeleteManualElement()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        Step step = ManualManager.GetInstance().GetCurrentStep();
        Substep substep = ManualManager.GetInstance().GetCurrentSubstep();

        if (substep == null)
            return;
        int index = step.GetSubstepIndex(substep);
        int newIndex;

        if (step.GetSubsteps().Count <= 1)
        {
            Debug.Log("Was passiert beim Löschen des letzten Substeps?");
            return;
        }

        if (index > 0)
        {
            newIndex = index - 1;
        }
        else
        {
            newIndex = index + 1;
        }

        step.GetSubsteps().Remove(substep);
        ManualManager.GetInstance().SetCurrentSubstep(step.GetSubsteps()[newIndex]);
        Destroy(manualElements[index]);

    }

    public override GameObject GetElementsPanel(GameObject manualElement)
    {
        return manualElement;
    }

    public override GameObject GetImageObject(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Text GetElementsTitle(GameObject imageObject)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificHighlight(GameObject manualElement)
    {
        GetElementsPanel(manualElement).GetComponent<Image>().color = UIManager.GetInstance().GetGreen();
        SelectManualElement(manualElement);
    }

    public override void TypeSpecificDeselection(GameObject manualElement, int i)
    {

    }

    public override GameObject GetManualElement(PointerEventData eventData)
    {
        return eventData.selectedObject;
    }

    public override GameObject InstantiateManualElement()
    {
        GameObject substepElement = (GameObject)GameObject.Instantiate(Resources.Load("UI/Editor/Substep"), Vector3.zero, new Quaternion(0, 0, 0, 0));
        SetManualElementToScrollerContent(substepElement);
        GetElementsPanel(substepElement).AddComponent<SelectManualElement>();
        GetElementsPanel(substepElement).GetComponent<SelectManualElement>().SetScrollPanelController(this);
        
        return substepElement;
    }

    public override void CreateNewManualElementDatastructure(GameObject manualElement)
    {
        ManualManager manualManager = ManualManager.GetInstance();
        Instruction currentInstruction = manualManager.GetCurrentInstruction();
        Step step = ManualManager.GetInstance().GetCurrentStep();
        Substep substep = step.CreateNewSubstep(step.GetSubsteps().Count - 1);
        manualElement.name += step.GetSubsteps().Count;
        manualElement.GetComponentInChildren<Text>().text = "" + step.GetSubsteps().Count;
        manualManager.SetCurrentSubstep(substep);
    }

    public override GameObject InstantiateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public override void CreateNewManualElementDatastructure(int index, GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }
}
