﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//#Basis Class, each derived class controls the gameObjects to a specific Datatype and manages it's representation

public abstract class ScrollPanelController : MonoBehaviour
{
    protected List<GameObject> manualElements = new List<GameObject>();
    protected bool menuElementDragged;

    protected Color32 deselectedColor = Color.gray;
    protected Color32 markerColor = new Color32(253, 174, 89, 255);

    public void RefreshListView()
    {
        GameObject content = GetContent();

        foreach (GameObject manualElement in manualElements)
        {
            manualElement.transform.SetParent(Camera.main.transform);
        }

        foreach (GameObject manualElement in manualElements)
        {
            manualElement.transform.parent = content.transform;
            manualElement.transform.position = new Vector3(manualElement.transform.position.x, manualElement.transform.position.y, content.transform.position.z);
        }

    }

    public void HighlightManualElement(GameObject manualElement)
    {
        GameObject element;
        for (int i = 0; i < manualElements.Count; i++)
        {
            element = manualElements[i];

            if (element == null)
                continue;

            GetElementsPanel(element).GetComponent<Image>().color = deselectedColor;

            TypeSpecificDeselection(element, i);

        }
        TypeSpecificHighlight(manualElement);
    }

    public abstract void TypeSpecificDeselection(GameObject manualElement, int i);
    public abstract void TypeSpecificHighlight(GameObject manualElement);

    public void AddManualElement(GameObject manualElement, float width, float height)
    {
        int index = GetManualElementsIndex(manualElement);
        manualElements.Add(manualElement);
        //CreateManualElement(index+1);
        if (ApplicationManager.GetInstance().init)
            return;
        RefreshListView();
        HighlightManualElement(manualElement);
        ////ManualManager.GetInstance().GetCurrentHologrammInstruction().ComputeOriginPositionsAndRotations();
    }

    public void InsertListElement(int index, GameObject manualElement, float width, float height)
    {
        manualElement.transform.SetParent(GetContent().transform);
        manualElements.Insert(index, manualElement);
        RefreshListView();
        HighlightManualElement(manualElement);
        ////ManualManager.GetInstance().GetCurrentHologrammInstruction().ComputeOriginPositionsAndRotations();
    }

    //public void RemoveListElement(GameObject manualElement)
    //{
    //    for (int i = 0; i < manualElements.Count; i++)
    //    {
    //        if (Equals(manualElements[i], manualElement))
    //        {
    //            RemoveManualElementAt(i);
    //            RefreshListView();
    //            break;
    //        }
    //    }
    //}

    public void RemoveManualElement(GameObject manualElement)
    {

        for (int i = 0; i < manualElements.Count; i++)
        {
            if (Equals(manualElements[i], manualElement))
            {
                GameObject element = manualElements[i];
                ManualManager.GetInstance().GetCurrentSubstep().GetMicrosteps().RemoveAt(i);
                manualElements.RemoveAt(i);
                DestroyManualElement(manualElement);
                RefreshListView();
            }
        }
        ////ManualManager.GetInstance().GetCurrentHologrammInstruction().ComputeOriginPositionsAndRotations();
    }

    private void DestroyManualElements()
    {
        for (int i = 0; i < manualElements.Count; i++)
        {
            Destroy(manualElements[i]);
        }
    }

    private void DestroyManualElement(int index) { Destroy(manualElements[index]); }

    private void DestroyManualElement(GameObject listElement) { Destroy(listElement); }

    public void ChangeManualElementsPosition(int dropIndex, GameObject manualElement)
    {
        manualElement.transform.SetSiblingIndex(dropIndex);
        FillManualElements();
    }

    public void FillManualElements()
    {
        manualElements.Clear();
        GameObject manualElement;
        for (int i = 0; i < GetContent().transform.childCount; i++)
        {
            manualElement = GetContent().transform.GetChild(i).gameObject;
            if (!manualElement.activeSelf)
                continue;
            manualElements.Add(manualElement);
            float width = GetElementsPanel(manualElement).GetComponent<RectTransform>().sizeDelta.x;
            float height = GetElementsPanel(manualElement).GetComponent<RectTransform>().sizeDelta.y;
        }

    }

    public int GetManualElementsIndex(GameObject manualElement)
    {
        for (int i = 0; i < manualElements.Count; i++)
        {
            if (Equals(manualElements[i], manualElement))
                return i;
        }

        return -1;
    }

    public int GetManualElementsSiblingIndex(GameObject manualElement)
    {
        GameObject element;
        for (int i = 0; i < GetContent().transform.childCount; i++)
        {
            element = GetContent().transform.GetChild(i).gameObject;
            if (Equals(element, manualElement))
                return i;
        }

        return -1;
    }


    public GameObject GetContent() { return this.transform.GetChild(0).GetChild(0).gameObject; }

    public abstract Button GetElementsAddButton(GameObject manualElement);

    public abstract Button GetElementsDeleteButton(GameObject manualElement);

    public List<GameObject> GetManualElements() { return manualElements; }

    public abstract GameObject GetElementsPanel(GameObject manualElement);

    public abstract GameObject GetImageObject(GameObject manualElement);

    public abstract Text GetElementsTitle(GameObject imageObject);

    public int GetDropIndex(GameObject closestManualElement)
    {
        GameObject manualElement;
        for (int i = 0; i < GetContent().transform.childCount; i++)
        {
            manualElement = GetContent().transform.GetChild(i).gameObject;
            if (Equals(manualElement, closestManualElement))
                return i;
        }
        return -1;
    }

    public GameObject GetClosestManualElementToPosition(Vector3 position, GameObject manualElementToIgnore)
    {
        GameObject closest = null;
        float closestDistance = 0;
        float tempDistance = 0;
        GameObject manualElement;

        if (GetContent().transform.childCount == 0)
            return null;

        for (int i = 0; i < GetContent().transform.childCount; i++)
        {
            manualElement = GetContent().transform.GetChild(i).gameObject;
            if (!manualElement.activeSelf)
                continue;
            tempDistance = Vector3.Distance(manualElement.transform.position, position);
            if (closest == null)
            {
                closest = manualElement;
                closestDistance = tempDistance;

                if (manualElements.Count == 1)
                    return closest;
                else
                    continue;
            }
            if (Equals(manualElement, manualElementToIgnore))
                continue;


            if (tempDistance < closestDistance || Equals(closest, manualElementToIgnore))
            {
                closest = manualElement;
                closestDistance = tempDistance;
            }
        }

        return closest;
    }


    public void SetManualElementToScrollerContent(GameObject manualElement)
    {
        GameObject content = GetContent();
        manualElement.transform.SetParent(content.transform);
        manualElement.transform.position = content.transform.position;
        manualElement.transform.rotation = content.transform.rotation;
        manualElement.transform.localScale = content.transform.localScale;
    }
    public abstract GameObject InstantiateManualElement();
    public abstract GameObject InstantiateManualElement(int index);
    public abstract void CreateNewManualElementDatastructure(GameObject manualElement);
    public abstract void CreateNewManualElementDatastructure(int index, GameObject manualElement);
    public abstract GameObject CreateManualElement(int index);
    public abstract GameObject CreateManualElement();
    public abstract GameObject CreateManualElement(int index, InstructionType instructionType);
    public abstract void SelectManualElement(GameObject manualElement);
    public abstract GameObject GetManualElement(PointerEventData eventData);
}
