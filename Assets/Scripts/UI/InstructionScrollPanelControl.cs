﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InstructionScrollPanelControl : ScrollPanelController
{
    public override GameObject CreateManualElement(int index, InstructionType instructionType)
    {
        GameObject result = null;
        if (ApplicationManager.GetInstance().IsEditor())
        {
            ManualManager manualManager = ManualManager.GetInstance();
            Manual currentManual = manualManager.GetCurrentManual();

            switch (instructionType)
            {
                case InstructionType.TEXT:
                    currentManual.CreateNewTextInstruction(index);
                    break;
                case InstructionType.PICTURE:
                    currentManual.CreateNewPictureInstruction(index);
                    break;
                case InstructionType.VIDEO:
                    currentManual.CreateNewVideoInstruction(index);
                    break;
                case InstructionType.HOLOGRAM:
                    currentManual.CreateNewHologramInstruction(index);
                    break;
                default:
                    break;
            }
        }
        else
        {

        }
        return result;
    }

    public override GameObject CreateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject CreateManualElement()
    {
        throw new System.NotImplementedException();
    }

    public override void CreateNewManualElementDatastructure(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override void CreateNewManualElementDatastructure(int index, GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsAddButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Button GetElementsDeleteButton(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetElementsPanel(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override Text GetElementsTitle(GameObject imageObject)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetImageObject(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject GetManualElement(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public override GameObject InstantiateManualElement()
    {
        throw new System.NotImplementedException();
    }

    public override GameObject InstantiateManualElement(int index)
    {
        throw new System.NotImplementedException();
    }

    public override void SelectManualElement(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificDeselection(GameObject manualElement, int i)
    {
        throw new System.NotImplementedException();
    }

    public override void TypeSpecificHighlight(GameObject manualElement)
    {
        throw new System.NotImplementedException();
    }

}
