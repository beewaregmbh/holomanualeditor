﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("Substep")]
public abstract class Substep 
{
    [XmlArray("microsteps")]
    [XmlArrayItem("ComponentDerivedClass1", typeof(TextMicrostep))]
    [XmlArrayItem("ComponentDerivedClass2", typeof(PictureMicrostep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(VideoMicrostep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramMicrostep))]

    public List<Microstep> microsteps = new List<Microstep>();

    public abstract Microstep CreateNewMicrostep(int index);
    public abstract Microstep CreateNewMicrostep();   
    public List<Microstep> GetMicrosteps() { return microsteps; }
    public Substep() { }

    public int GetMicrostepIndex(Microstep selectedMicrostep)
    {
        Microstep microstep;
        for (int i = 0; i < microsteps.Count; i++)
        {
            microstep = microsteps[i];

            if (Equals(microstep, selectedMicrostep))
                return i;
        }
        return -1;
    }
}
