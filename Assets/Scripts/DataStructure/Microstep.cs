﻿using System.Collections.Generic;
using System.Xml.Serialization;

/*
 * Basis class for Microsteps. Each Microstep has a list of Modules.
 */
[XmlRoot("Microstep")]

public abstract class Microstep
{
    public Microstep() { }
    [XmlArray("modules")]
    [XmlArrayItem("ComponentDerivedClass1", typeof(TextModule))]
    [XmlArrayItem("ComponentDerivedClass2", typeof(PictureModule))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(VideoModule))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramModule))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramRotationModule))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramTranslationModule))]

    public List<Module> modules = new List<Module>();
    public abstract Module CreateNewModule(int index);
    public List<Module> GetModules() { return modules; }
    public abstract Module CreateNewModule(bool isRotationModul);

}