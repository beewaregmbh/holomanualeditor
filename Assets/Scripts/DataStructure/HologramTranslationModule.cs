﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
[XmlInclude(typeof(Module))]
/*
 * This module describes the translation of a 3D-model. 
*/
public class HologramTranslationModule : HologramModule
{
    public float scalingFactor = 1;
    public static float STD_TIME_PER_TRANSLATION = 1;
    public Vector3 offsetVector;
    public int dropdownTranslationAxis_value;
    public int dropdownDirection_value;
    public float distance;
    public Vector3 translationAxis;
    public Vector3 destinationPosition;
    public Vector3 translationVector;
    public float translationSpeed = 1f;


    public HologramTranslationModule()
    {

    }
    public override void OnTransform(GameObject modelpart, float deltaAnimationTime)
    {
        HologramSubstep substep = (HologramSubstep)ManualManager.GetInstance().GetCurrentSubstep();
        modelpart.transform.position += translationVector * deltaAnimationTime;
    }

    public void SetValuesToStandard()
    {
        dropdownTranslationAxis_value = 2;
        dropdownDirection_value = 1;
        distance = 500;
        translationSpeed = 30;
        SetTranslationAxis();
    }

    public void SetTranslationAxis()
    {
        switch (dropdownTranslationAxis_value)
        {
            case 0:
                translationAxis = Vector3.right;
                break;
            case 1:
                translationAxis = Vector3.down;
                break;
            case 2:
                translationAxis = Vector3.forward;
                break;
            case 3:
                break;
            default:
                Debug.Log("cs:anim_Rotation, setVector(),line 82, error by setting vector");
                break;
        }

        if (dropdownDirection_value == 1)
            translationAxis = new Vector3(-translationAxis.x, -translationAxis.y, -translationAxis.z);

        SetTranslateTerminationCondition();
    }

    public void SetTranslateTerminationCondition()
    {
        destinationPosition = new Vector3(GetOffsetVector().x * distance, GetOffsetVector().y * distance, GetOffsetVector().z * distance);
        translationVector = new Vector3(destinationPosition.x / (translationSpeed), destinationPosition.y / (translationSpeed), destinationPosition.z / (translationSpeed));
    }

    public Vector3 GetOffsetVector()
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        float factor = hologramInstruction.GetHologramsModelparts()[0].transform.lossyScale.x;
        float x = translationAxis.x * factor;
        float y = translationAxis.y * factor;
        float z = translationAxis.z * factor;
        offsetVector = Vector3.zero;
        offsetVector = new Vector3(x, y, z);
        return offsetVector;
    }



    public override float GetAnimationTime()
    {
        return STD_TIME_PER_TRANSLATION * scalingFactor * distance;

    }

    public override void InitAnimationTime(float MaxAnimationTime)
    {
        throw new System.NotImplementedException();
    }
}
