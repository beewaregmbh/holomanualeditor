﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlInclude(typeof(HologramModule))]
[XmlInclude(typeof(HologramRotationModule))]
[XmlInclude(typeof(HologramTranslationModule))]
[XmlRoot("Module")]

public abstract class Module 
{
    public Module() { }
}
