﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
[XmlInclude(typeof(HologramRotationModule))]
[XmlInclude(typeof(HologramTranslationModule))]
public abstract class HologramModule : Module
{

    public float normTime;
    public Vector3 axis;
    public int counter;
    public int selectedModelpartsCount;
    public bool animationCompleted;

    public bool IsAnimationCompleted() { return animationCompleted; }
    public void SetAnimationCompleted(bool animationCompleted) { this.animationCompleted = animationCompleted; }
    public abstract void InitAnimationTime(float MaxAnimationTime);

    public HologramModule()
    {

    }

    public void ResetValues()
    {
        counter = 0;
    }

    public void SetNormTime(float normTime) { this.normTime = normTime; }
    public abstract float GetAnimationTime();
    public abstract void OnTransform(GameObject modelpart, float deltaAnimationTime);
}
