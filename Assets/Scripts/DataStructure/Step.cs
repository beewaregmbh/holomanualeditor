﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("Step")]
public abstract class Step
{
    [XmlArray("substeps")]
    [XmlArrayItem("ComponentDerivedClass1", typeof(TextSubstep))]
    [XmlArrayItem("ComponentDerivedClass2", typeof(PictureSubstep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(VideoSubstep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramSubstep))]

    public List<Substep> substeps = new List<Substep>();
    public long ID;

    public Step() { }
    public abstract Substep CreateNewSubstep(int index);
    public long GetID() { return ID; }
    public void SetID(long ID) { this.ID = ID; }

    public int GetSubstepIndex(Substep selectedSubstep)
    {
        Substep substep;
        for (int i = 0; i < substeps.Count; i++)
        {
            substep = substeps[i];
            if (Equals(substep, selectedSubstep))
                return i;
        }
        return -1;
    }

    public List<Substep> GetSubsteps() { return substeps; }
}