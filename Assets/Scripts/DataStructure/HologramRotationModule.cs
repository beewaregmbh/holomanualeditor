﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
/*
 * This module describes the rotation of a 3D-model. 
*/
public class HologramRotationModule : HologramModule
{
    public static float STD_ROTATION_PER_SECOND = 0.1f;
    public int dropdownRotation_value;
    public int dropdownRotationAxis_value;
    public int dropdownClockwise_value;
    public float numberOrAngle;
    public float angle;
    public float rotationSpeed = 1f;
    public Vector3 rotOffsetVector;
    public Vector3 rotationVector;
    public int pivot_X = 0; //-1:Left, 0 Middle, 1 Right;
    public int pivot_Y = 0;
    public int pivot_Z = 0;
    public int pivotObjectindex = -1;
    public float rot_value;
    private List<Vector3> pivotPoints = new List<Vector3>();

    EditHologramModule edit;

    public HologramRotationModule()
    {

    }

    public override void OnTransform(GameObject modelpart, float executionScalar)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        Vector3 rotVector = GetRotOffsetVector();
        SetModelPartsPivots(ManualManager.GetInstance().GetCurrentSubstep());
        float tempAngle = angle * executionScalar;
        modelpart.transform.RotateAround(GetModelPartsPivot(ManualManager.GetInstance().GetCurrentSubstep(), modelpart), rotVector, tempAngle);

    }


    public void SetValuesToStandard()
    {
        dropdownRotation_value = 1;
        angle = 45;
        dropdownRotationAxis_value = 2;
        dropdownClockwise_value = 0;
        rotationSpeed = 20;
        SetRotationVector();
    }

    public void SetRotationVector()
    {
        switch (dropdownRotationAxis_value)
        {
            case 0:
                rotationVector = Vector3.right;
                break;
            case 1:
                rotationVector = Vector3.up;
                break;
            case 2:
                rotationVector = Vector3.forward;
                break;
            case 3:
                break;
            default:
                Debug.Log("cs:anim_Rotation, setVector(),error by setting vector");
                break;
        }
    }
    public Vector3 GetRotOffsetVector()
    {
        int direction;
        if (dropdownClockwise_value == 1)
            direction = 1;
        else
            direction = -1;

        return new Vector3(rotationVector.x * direction, rotationVector.y * direction, rotationVector.z * direction);
    }

    public Vector3 GetModelPartsPivot(Substep substep, GameObject modelpart)
    {
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> complete_model_List = hologramInstruction.GetHologramsModelparts();
        pivotPoints.Clear();
        GameObject obj;
        Vector3 center;

        if (pivotObjectindex == -1)
            obj = modelpart.gameObject;
        else
            obj = complete_model_List[pivotObjectindex];

        Renderer renderer = obj.transform.GetChild(0).GetComponent<Renderer>();

        if (renderer == null)
            return Vector3.zero;
        center = obj.transform.position;
        Vector3 pivot = center;

        switch (pivot_X)
        {
            case -1:
                pivot.x = center.x - (float)0.5 * renderer.bounds.size.x;
                break;
            case 0:
                pivot.x = center.x;
                break;
            case 1:
                pivot.x = center.x + (float)0.5 * renderer.bounds.size.x;
                break;
            default:
                break;
        }

        switch (pivot_Y)
        {
            case -1:
                pivot.y = center.y - (float)0.5 * renderer.bounds.size.y;
                break;
            case 0:
                pivot.y = center.y;
                break;
            case 1:
                pivot.y = center.y + (float)0.5 * renderer.bounds.size.y;
                break;
            default:
                break;
        }

        switch (pivot_Z)
        {
            case -1:
                pivot.z = center.z - (float)0.5 * renderer.bounds.size.z;
                break;
            case 0:
                pivot.z = center.z;
                break;
            case 1:
                pivot.z = center.z + (float)0.5 * renderer.bounds.size.z;
                break;
            default:
                break;
        }
        pivotPoints.Add(pivot);
        return pivot;
    }

    public void SetModelPartsPivots(Substep substep)
    {
        GameObject modelpart;
        HologramInstruction hologramInstruction = ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> complete_model_List = hologramInstruction.GetHologramsModelparts();
        pivotPoints.Clear();

        for (int i = 0; i < complete_model_List.Count; i++)
        {
            modelpart = complete_model_List[i];
            GetModelPartsPivot(substep, modelpart);
        }

    }

    public override float GetAnimationTime()
    {
        float result = 0;

        switch (dropdownRotation_value)
        {
            case 0:
                result = -1;
                break;
            case 1:
                result = (angle / (360 * STD_ROTATION_PER_SECOND)) * 1000;
                break;
            case 2:
                result = angle * STD_ROTATION_PER_SECOND;

                break;
            default:
                break;
        }
        return result;
    }

    public override void InitAnimationTime(float MaxAnimationTime)
    {
        if (dropdownRotation_value != 0)
            return;

        angle = MaxAnimationTime / 1000 * STD_ROTATION_PER_SECOND * 360;
    }
}

