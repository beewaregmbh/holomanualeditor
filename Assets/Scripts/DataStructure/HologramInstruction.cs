﻿using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[XmlInclude(typeof(Instruction))]
public class HologramInstruction : Instruction
{
    private Quaternion currentRotation;
    private Vector3 currentPosition;

    private List<Quaternion> originRotations = new List<Quaternion>();
    private List<Vector3> originPositions = new List<Vector3>();

    public string hologramsRelativeRessourcePath;
    private GameObject hologram;
    private Vector3 originPosition;
    private Quaternion originRotation;
    private List<GameObject> hologramsModelparts = new List<GameObject>();

    public List<Vector3> GetOriginPositions() { return originPositions; }
    public List<Quaternion> GetOriginRotations() { return originRotations; }
    public Vector3 GetOriginPosition() { return originPosition; }
    public void SetOriginPosition(Vector3 originPosition) { this.originPosition = originPosition; }
    public Quaternion GetOriginRotation() { return originRotation; }
    public void SetOriginRotation(Quaternion originRotation) { this.originRotation = originRotation; }

    public override Step CreateNewStep(int index)
    {
        Step result = new HologramStep();
        steps.Insert(index, result);
        result.SetID(ManualManager.GetInstance().GetNextStepID());
        return result;
    }

    public void FillHologramsModelparts()
    {
        int count = hologram.transform.childCount;
        hologramsModelparts.Clear();
        GameObject modelpart;
        for (int i = 0; i < count; i++)
        {
            modelpart = hologram.transform.GetChild(i).gameObject;

            if (modelpart == null)
                continue;

            hologramsModelparts.Add(modelpart);
        }
    }

    public void SetHologram(GameObject hologram) { this.hologram = hologram; }
    public GameObject GetHologram() { return hologram; }

    public void SetHologramsRelativeRessourcePath(string hologramsRelativeRessourcePath) { this.hologramsRelativeRessourcePath = hologramsRelativeRessourcePath; }

    public List<GameObject> GetHologramsModelparts() { return hologramsModelparts; }

    public void ComputeOriginPositionsAndRotations()
    {
        List<HologramInstruction> hologramInstructions = new List<HologramInstruction>();
        originRotations.Clear();
        originPositions.Clear();

        foreach (GameObject modelpart in hologramsModelparts)
        {
            originRotations.Add(modelpart.transform.rotation);
            originPositions.Add(modelpart.transform.position);
        }

        originPosition = hologram.transform.position;
        originRotation = hologram.transform.rotation;
    }

    public void PreTransform()
    {
        currentRotation = new Quaternion(hologram.transform.rotation.x, hologram.transform.rotation.y, hologram.transform.rotation.z, hologram.transform.rotation.w);
        currentPosition = new Vector3(hologram.transform.position.x, hologram.transform.position.y, hologram.transform.position.z);
        hologram.transform.rotation = originRotation;
        hologram.transform.position = originPosition;
    }

    public void PostTransform()
    {

        hologram.transform.rotation = currentRotation;
        hologram.transform.position = currentPosition;
    }
}
