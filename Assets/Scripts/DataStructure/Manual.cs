﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("Manual")]
public class Manual  {

    [XmlArray("Instructions")]
    [XmlArrayItem("ComponentDerivedClass1", typeof(TextInstruction))]
    [XmlArrayItem("ComponentDerivedClass2", typeof(PictureInstruction))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(VideoInstruction))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramInstruction))]

    public List<Instruction> instructions = new List<Instruction>();

    public List<Instruction> GetInstructions() { return instructions; }

    public Manual() { }

    public TextInstruction CreateNewTextInstruction(int index)
    {
        TextInstruction result = new TextInstruction();
        instructions.Insert(index, result);
        return result;
    }

    public PictureInstruction CreateNewPictureInstruction(int index)
    {
        PictureInstruction result = new PictureInstruction();
        instructions.Insert(index, result);
        return result;
    }

    public VideoInstruction CreateNewVideoInstruction(int index)
    {
        VideoInstruction result = new VideoInstruction();
        instructions.Insert(index, result);
        return result;
    }

    public HologramInstruction CreateNewHologramInstruction(int index)
    {
        HologramInstruction result = new HologramInstruction();
        instructions.Insert(index, result);
        ManualManager.GetInstance().SetCurrentInstruction( result);
        return result;        
    }
    
}
