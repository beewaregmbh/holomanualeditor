﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramSubstep : Substep
{
    private List<Vector3> modelpartsDemontageOriginPositions;
    private List<Quaternion> modelpartsDemontageOriginRotation;
    public List<string> selectedModelpartsNames = new List<string>();
    private List<GameObject> selectedModelparts = new List<GameObject>();

    public void SetModelparts(List<string> modelparts) { this.selectedModelpartsNames = modelparts; }
    public List<string> GetSelectedModelpartsNames() { return selectedModelpartsNames; }
    public List<GameObject> GetSelectedModelparts() { return selectedModelparts; }

    public override Microstep CreateNewMicrostep(int index)
    {
        Microstep result = new HologramMicrostep();
        microsteps.Insert(index, result);
        return result;
    }

    public override Microstep CreateNewMicrostep()
    {
        Microstep result = new HologramMicrostep();
        microsteps.Add(result);
        return result;
    }
}