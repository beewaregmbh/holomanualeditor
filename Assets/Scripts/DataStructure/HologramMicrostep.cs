﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//#basis class for the datastructure HologramMicrostep. 

public class HologramMicrostep : Microstep
{
    protected List<Quaternion> originRotations = new List<Quaternion>();
    protected List<Vector3> originPositions = new List<Vector3>();

    bool animationCompleted = false;
    public float animationTime;
    DateTime startTime;

    public HologramMicrostep() { }

    public List<Quaternion> GetOriginRotations() { return originRotations; }
    public List<Vector3> GetOriginPositions() { return originPositions; }

    public bool IsAnimationCompleted() { return animationCompleted; }

    public void SetAnimationTime()
    {
        float result = -1;

        foreach (HologramModule module in modules)
        {
            if (module.GetAnimationTime() > result)
                result = module.GetAnimationTime();
        }

        animationTime = result;
    }
    public override Module CreateNewModule(bool isRotationModul)
    {
        HologramModule module;
        if (isRotationModul)
        {
            HologramRotationModule rotModule = new HologramRotationModule();
            modules.Add(rotModule);
            rotModule.SetValuesToStandard();
            rotModule.SetModelPartsPivots(ManualManager.GetInstance().GetCurrentSubstep());
            module = rotModule;
        }
        else
        {
            HologramTranslationModule transModule = new HologramTranslationModule();
            modules.Add(transModule);
            transModule.SetValuesToStandard();
            module = transModule;
        }
        ManualManager.GetInstance().ComputeDemonatageOriginPosition();
        return module;
    }

    public override Module CreateNewModule(int index)
    {
        throw new System.NotImplementedException();
    }

    public void TransformModel(bool oneStep, HologramSubstep substep)
    {

        HologramInstruction hologramInstruction = (HologramInstruction)ManualManager.GetInstance().GetCurrentHologrammInstruction();
        List<GameObject> hologramsModelparts = hologramInstruction.GetHologramsModelparts();
        double executionScalar = 1;

        /* For the montage Animation the demontage Animation is inverted. Therefore the modules transformation of an microstep
         * have to finish on the same time.
         * Otherwise start and end point in montage and demontage are not the same and the movement is not correct inverted.
         * This is why the values are standardised according the longest execution time of the modules of one microstep.
        */
        if (!oneStep)
        {
            TimeSpan diff = DateTime.Now - startTime;
            double deltaTime = diff.TotalMilliseconds;
            executionScalar = deltaTime / animationTime;
            if (deltaTime >= animationTime)
            {
                animationCompleted = true;
                return;
            }
        }


        if (ApplicationManager.GetInstance().IsMontage())
            executionScalar = 1 - executionScalar;

        GameObject hologram = hologramInstruction.GetHologram();
        GameObject modelpart;

        List<bool> selectedModelpartBoolList = UIManager.GetInstance().FillSelectedModelpartBoolListFromSubstep(substep);
        //# the modelparts are set to their startpositions
        //ManualManager.GetInstance().ResetHologramToInitalTransform();
        hologramInstruction.PreTransform();
        for (int j = 0, count = substep.GetSelectedModelparts().Count; j < count; j++)
        {
            modelpart = substep.GetSelectedModelparts()[j];
            modelpart.transform.position = originPositions[j];
            modelpart.transform.rotation = originRotations[j];
        }
        //# the modelparts are transformed as it is saved in the modules
        foreach (HologramModule module in modules)
        {
            for (int j = 0, count = substep.GetSelectedModelparts().Count; j < count; j++)
            {
                modelpart = substep.GetSelectedModelparts()[j];

                if (Equals(modelpart.transform.parent.name, "GameObject"))
                    module.OnTransform(modelpart.transform.parent.gameObject, (float)executionScalar);
                else
                    module.OnTransform(modelpart, (float)executionScalar);

            }

        }
        hologramInstruction.PostTransform();
    }

    public void EnableTransform()
    {
        startTime = DateTime.Now;
        animationCompleted = false;
        SetAnimationTime();
    }


}