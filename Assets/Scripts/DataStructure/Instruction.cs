﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
[XmlRoot("Instruction")]

public abstract class Instruction
{
    [XmlArray("steps")]
    [XmlArrayItem("ComponentDerivedClass1", typeof(TextStep))]
    [XmlArrayItem("ComponentDerivedClass2", typeof(PictureStep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(VideoStep))]
    [XmlArrayItem("ComponentDerivedClass3", typeof(HologramStep))]

    public List<Step> steps = new List<Step>();
    public long ID;
    public abstract Step CreateNewStep(int index);

    public Instruction() { }

    public long GetID() { return ID; }

    public void SetID(long ID) { this.ID = ID; }

    public List<Step> GetSteps() { return steps; }

    public int GetStepIndex(Step selectedStep)
    {
        Step step;

        for (int i = 0; i < steps.Count; i++)
        {
            step = steps[i];
            if (Equals(step, selectedStep))
                return i;
        }

        return -1;
    }
}

