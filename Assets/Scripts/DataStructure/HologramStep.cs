﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramStep : Step
{
    public override Substep CreateNewSubstep(int index)
    {
        Substep result = new HologramSubstep();
        substeps.Add(result);
        return result;
    }
}
