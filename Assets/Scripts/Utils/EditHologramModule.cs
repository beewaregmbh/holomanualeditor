﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EditHologramModule : MonoBehaviour, IPointerClickHandler
{
    public bool rotation;
    public void OnPointerClick(PointerEventData eventData)
    {
        UIManager.GetInstance().SwitchViewInHologramInstruction();
    }
}
