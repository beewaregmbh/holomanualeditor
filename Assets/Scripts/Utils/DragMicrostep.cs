﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragMicrostep : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler
{
    //# Has to be added to ModuleButtons. 

    public MicrostepScrollPanelControl microstepScrollPanelControl;
    private int dragIndex;
    private int dropIndex;

    GameObject tempModule;
    public bool tempIsRotationModule;


    public void OnBeginDrag(PointerEventData eventData)
    {
        tempModule = eventData.selectedObject;

        if (tempModule.name.Contains("Rotation"))
            tempIsRotationModule = true;
        else
            tempIsRotationModule = false;

        tempModule.transform.GetChild(0).gameObject.SetActive(true);
        UIManager.GetInstance().SetTempModuleSelected(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(UIManager.GetInstance().canvas.transform as RectTransform, Input.mousePosition, UIManager.GetInstance().canvas.worldCamera, out pos);
        pos = UIManager.GetInstance().canvas.transform.TransformPoint(pos);
        microstepScrollPanelControl.AddTempModuleToClosestOrNewMicrostep(pos, tempModule);
        tempModule.transform.position = pos;
        tempModule.transform.localPosition = new Vector3(tempModule.transform.localPosition.x, tempModule.transform.localPosition.y, 0);

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GameObject microstep = tempModule.transform.parent.parent.parent.gameObject;
        ModuleScrollPanelControl moduleScrollPanelControl = microstep.GetComponent<ModuleScrollPanelControl>();
        int index = microstepScrollPanelControl.GetManualElementsIndex(microstep);
        ManualManager.GetInstance().SetCurrentMicrostep(ManualManager.GetInstance().GetCurrentSubstep().GetMicrosteps()[index]); 
        moduleScrollPanelControl.CreateManualElement(tempIsRotationModule);
        ResetTempModule();
        microstepScrollPanelControl.RemoveTempMicrostep();
    }

    public void ResetTempModule()
    {
        tempModule.transform.SetParent(UIManager.GetInstance().microstepModificationView.transform);

        if (tempIsRotationModule)
            tempModule.transform.position = UIManager.GetInstance().rotationModulSpawner.transform.position;
        else
            tempModule.transform.position = UIManager.GetInstance().translationModulSpawner.transform.position;

        tempModule.transform.GetChild(0).gameObject.SetActive(false);
        tempModule = null;
        microstepScrollPanelControl.SetTempMicrostep(null);
    }

    public void SetScrollPanelController(MicrostepScrollPanelControl microstepScrollPanelControl) { this.microstepScrollPanelControl = microstepScrollPanelControl; }

}
