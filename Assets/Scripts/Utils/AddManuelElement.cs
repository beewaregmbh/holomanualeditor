﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AddManuelElement : MonoBehaviour, IPointerClickHandler
{
    public ScrollPanelController scrollPanelController;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject gameObject = eventData.selectedObject.transform.parent.gameObject;
        scrollPanelController.AddManualElement(gameObject, gameObject.GetComponent<RectTransform>().sizeDelta.x, gameObject.GetComponent<RectTransform>().sizeDelta.y);
    }

    public void SetScrollPanelController(ScrollPanelController scrollPanelController)  { this.scrollPanelController = scrollPanelController;}
}
