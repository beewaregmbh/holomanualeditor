﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//# script has to be added to manualElements like steps. Hereby they can be swapped if they are dragged and dropped and highlighted if they are clicked.

public class SelectManualElement : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler, IPointerClickHandler
{
    private ScrollPanelController scrollPanelController;
    private int dragIndex;
    private int dropIndex;
    GameObject selectedManualElement;

    public void OnBeginDrag(PointerEventData eventData)
    {
        selectedManualElement = scrollPanelController.GetManualElement(eventData);
        scrollPanelController.HighlightManualElement(selectedManualElement);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(UIManager.GetInstance().canvas.transform as RectTransform, Input.mousePosition, Camera.main, out pos); //UIManager.GetInstance().canvas.worldCamera, out pos);
        selectedManualElement.transform.position = UIManager.GetInstance().canvas.transform.TransformPoint(pos);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(UIManager.GetInstance().canvas.transform as RectTransform, Input.mousePosition, Camera.main, out pos);
        pos = UIManager.GetInstance().canvas.transform.TransformPoint(pos);
        dragIndex = scrollPanelController.GetManualElementsIndex(selectedManualElement);
        GameObject nearestManualElement = scrollPanelController.GetClosestManualElementToPosition(pos,selectedManualElement);
        dropIndex = scrollPanelController.GetDropIndex(nearestManualElement);

        if (dropIndex == -1 || dragIndex == -1)
        {
            scrollPanelController.RefreshListView();
            return;
        }

        scrollPanelController.ChangeManualElementsPosition(dropIndex, selectedManualElement);
        scrollPanelController.RefreshListView();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject gameObject = scrollPanelController.GetManualElement(eventData);
        scrollPanelController.HighlightManualElement(gameObject);
    }

    public void SetScrollPanelController(ScrollPanelController scrollPanelController) { this.scrollPanelController = scrollPanelController; }
}
