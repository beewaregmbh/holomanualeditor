﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DeleteManualElement : MonoBehaviour, IPointerClickHandler
{
    private ScrollPanelController scrollPanelController;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject manualElement = eventData.selectedObject.transform.parent.gameObject;
        scrollPanelController.RemoveManualElement(manualElement);
    }

    public void SetScrollPanelController(ScrollPanelController scrollPanelController)
    {
        this.scrollPanelController = scrollPanelController;
    }
}
