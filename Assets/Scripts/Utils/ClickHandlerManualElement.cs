﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickHandlerManualElement : MonoBehaviour, IPointerClickHandler
{
    private ScrollPanelController scrollPanelController;

    public void OnPointerClick(PointerEventData eventData)
    {
        scrollPanelController.CreateManualElement(scrollPanelController.GetManualElementsIndex(eventData.selectedObject)+1);
    }

    public void SetScrollPanelController(ScrollPanelController scrollPanelController)
    {
        this.scrollPanelController = scrollPanelController;
    }
}
